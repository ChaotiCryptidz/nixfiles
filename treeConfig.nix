{}: {
  folder = ./.;
  config = {
    "hosts/*".functor.enable = true;
    "users/*".functor.enable = true;
    "home/*".functor.enable = true;

    "profiles/nixos/*".functor.enable = true;
    "profiles/home-manager/*".functor.enable = true;

    "presets/nixos/*".functor.enable = true;

    "modules/nixos" = {
      functor = {
        enable = true;
        external = [];
      };
    };

    "modules/home" = {
      functor = {
        enable = true;
        external = [];
      };
    };
  };
}
