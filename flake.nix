{
  description = "NixOS configuration";

  inputs = {
    # flake-utils which everyone uses for default systems list
    flake-utils.url = "github:numtide/flake-utils";
    # flake-compat which is used for non-flake stuff but added anyway to reduce dupes
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # nixpkgs Unstable Inputs
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    nixpkgs-kodi-inputstream-adaptive-fork.url = "github:chaoticryptidz/nixpkgs/patch-4";

    home-manager-unstable.url = "github:nix-community/home-manager";
    home-manager-unstable.inputs.nixpkgs.follows = "nixpkgs-unstable";

    tree-input.url = "github:kittywitch/tree";
    tree-input.inputs.nixpkgs.follows = "nixpkgs-unstable";

    nur.url = "github:nix-community/NUR";

    vaultui.url = "git+https://forgejo.owo.monster/chaos/VaultUI";
    #vaultui.inputs.nixpkgs.follows = "nixpkgs-unstable";
    vaultui.inputs.utils.follows = "flake-utils";
    vaultui.inputs.flake-compat.follows = "flake-compat";

    musicutil.url = "git+https://forgejo.owo.monster/chaos/musicutil";
    #musicutil.inputs.nixpkgs.follows = "nixpkgs-unstable";
    musicutil.inputs.utils.follows = "flake-utils";
    musicutil.inputs.flake-compat.follows = "flake-compat";

    food-site.url = "git+https://forgejo.owo.monster/chaos/food-site";
    #food-site.inputs.nixpkgs.follows = "nixpkgs-unstable";
    food-site.inputs.utils.follows = "flake-utils";
    food-site.inputs.flake-compat.follows = "flake-compat";
  };

  outputs = inputs: import ./outputs.nix inputs;
}
