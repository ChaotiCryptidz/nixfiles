{
  stdenv,
  unzip,
}:
stdenv.mkDerivation {
  name = "comic-code-1.0";

  srcs = ./Comic_Code.zip;
  sourceRoot = ".";

  nativeBuildInputs = [unzip];

  installPhase = ''
    mkdir -p $out/share/fonts
    unzip -j $srcs \*.otf -d $out/share/fonts/truetype
  '';
}
