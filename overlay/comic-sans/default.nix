{
  stdenv,
  unzip,
}:
stdenv.mkDerivation {
  name = "comic-sans-1.0";

  srcs = ./Comic_Sans.zip;
  sourceRoot = ".";

  nativeBuildInputs = [unzip];

  installPhase = ''
    mkdir -p $out/share/fonts
    unzip -j $srcs \*.ttf -d $out/share/fonts/truetype
  '';
}
