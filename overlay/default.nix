final: prev: rec {
  comic-sans = final.callPackage ./comic-sans {};
  comic-code = final.callPackage ./comic-code {};

  mk-enc-usb = final.callPackage ../extras/mk-enc-usb.nix {};
  mk-encrypted-drive = final.callPackage ../extras/mk-encrypted-drive.nix {};

  kitty-terminfo = final.runCommand "kitty-terminfo" {} ''
    mkdir -p $out/share
    cp -r ${./kitty-terminfo}/* $out/share
  '';

  vault = prev.vault-bin;

  linux_rpi5 = final.callPackage ./linux-rpi5.nix {
    kernelPatches = with final.kernelPatches; [
      bridge_stp_helper
      request_key_helper
    ];
  };

  linuxPackages_rpi5 = final.linuxPackagesFor linux_rpi5;

  raspberrypifw = prev.raspberrypifw.overrideAttrs (_oldAttrs: rec {
    version = "stable_20240529";

    src = final.fetchFromGitHub {
      owner = "raspberrypi";
      repo = "firmware";
      rev = "458df3adc11fccc3d26d3d6d8864738459290416";
      hash = "sha256-KsCo7ZG6vKstxRyFljZtbQvnDSqiAPdUza32xTY/tlA=";
    };
  });

  raspberrypiWirelessFirmware = prev.raspberrypiWirelessFirmware.overrideAttrs (_oldAttrs: rec {
    version = "unstable-2024-09-04";

    srcs = [
      (final.fetchFromGitHub {
        name = "bluez-firmware";
        owner = "RPi-Distro";
        repo = "bluez-firmware";
        rev = "78d6a07730e2d20c035899521ab67726dc028e1c";
        hash = "sha256-KakKnOBeWxh0exu44beZ7cbr5ni4RA9vkWYb9sGMb8Q=";
      })
      (final.fetchFromGitHub {
        name = "firmware-nonfree";
        owner = "RPi-Distro";
        repo = "firmware-nonfree";
        rev = "4b356e134e8333d073bd3802d767a825adec3807";
        hash = "sha256-T7eTKXqY9cxEMdab8Snda4CEOrEihy5uOhA6Fy+Mhnw=";
      })
    ];
  });

  raspberrypi-utils = final.callPackage ./raspberrypi-utils.nix {};

  widevine-aarch64-4k = final.callPackage ./widevine-aarch64 {pageSize = 4;};
  widevine-aarch64-16k = final.callPackage ./widevine-aarch64 {pageSize = 16;};

  mpd-headless =
    (prev.mpdWithFeatures.override {
      ffmpeg = final.ffmpeg_6-headless;
    }) {
      features = [
        "audiofile"
        "ffmpeg"
        "faad"
        "flac"
        "mpg123"
        "opus"
        "vorbis"
        "vorbisenc"
        "lame"
        "soxr"
        "libsamplerate"
        "libmpdclient"
        "id3tag"
        "expat"
        "pcre"
        "yajl"
        "icu"
        "sqlite"
        "systemd"
        "syslog"
        "io_uring"
        "curl"
        "nfs"
        "webdav"
      ];
    };
}
