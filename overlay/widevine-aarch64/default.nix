{
  stdenv,
  fetchurl,
  squashfsTools,
  python3,
  nspr,
  # Depends on kernel used, in kb
  pageSize ? 4,
}: let
  lacrosVersion = "128.0.6613.119";
in
  stdenv.mkDerivation {
    pname = "widevine-aarch64-${toString pageSize}k";
    version = "widevineVersion";

    srcs = fetchurl {
      url = "https://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/chromeos-lacros-arm64-squash-zstd-${lacrosVersion}";
      hash = "sha256-Q3g2npzSh9imzn36AR8EDWB5LS9MlhRBEqalNCLDxHI=";
    };

    unpackPhase = ":";
    sourceRoot = ".";

    buildPhase = ''
      unsquashfs -q $srcs 'WidevineCdm/*'
      env TARGET_PAGE_SIZE=${toString (pageSize * 1024)} python3 ${./widevine_fixup.py} squashfs-root/WidevineCdm/_platform_specific/cros_arm64/libwidevinecdm.so libwidevinecdm.so
    '';

    # TODO: systemwide install
    installPhase = ''
      install -d -m 0755 "$out"
      install -p -m 0755 -t "$out" libwidevinecdm.so
      install -p -m 0644 -t "$out" squashfs-root/WidevineCdm/manifest.json
      install -p -m 0644 -t "$out" squashfs-root/WidevineCdm/LICENSE

      mkdir -p "$out/share/google/chrome/WidevineCdm/_platform_specific/linux_arm64"
      mkdir -p "$out/share/google/chrome/WidevineCdm/_platform_specific/linux_x64"
      # Hack because Chromium hardcodes a check for this right now...
      touch "$out/share/google/chrome/WidevineCdm/_platform_specific/linux_x64/libwidevinecdm.so"
      ln -sf ../../../../manifest.json "$out/share/google/chrome/WidevineCdm"
      ln -sf ../../../../../../libwidevinecdm.so "$out/share/google/chrome/WidevineCdm/_platform_specific/linux_arm64/"
    '';

    nativeBuildInputs = [
      squashfsTools
      python3
    ];

    buildInputs = [
      nspr
    ];
  }
