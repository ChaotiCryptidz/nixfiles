rec {
  # Mountpoints
  mountpoint = "/usb";

  # Partition Labels
  encryptedPartLabel = "usb";
  unencryptedLabel = "usb_unencrypted";

  # Partition Filesystems
  unencryptedFSType = "ext4";

  # Mapper Information
  mapperName = "usb_unencrypted";
  preBootMapperName = "usb_unencrypted_preboot";

  # FS Paths
  encryptedPath = "/dev/disk/by-partlabel/${encryptedPartLabel}";
  unencryptedPath = "/dev/disk/by-label/${unencryptedLabel}";
  mapperPath = "/dev/mapper/${mapperName}";
  preBootMapperPath = "/dev/mapper/${preBootMapperName}";

  # Paths to some important files
  encryptionKeysPath = "${mountpoint}/encryption-keys";

  chaosAgePrivateKeyPath = "${mountpoint}/age-keys/chaoskey.priv";
  chaosAgePublicKeyPath = "${mountpoint}/age-keys/chaoskey.pub";

  sshPrivateKeyPath = "${mountpoint}/ssh-keys/chaos.priv";
  sshPublicKeyPath = "${mountpoint}/ssh-keys/chaos.pub";
}
