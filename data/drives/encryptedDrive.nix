# This works with both UEFI and BIOS based systems
rec {
  # Mountpoints
  mountpoint = "/";
  bootMountpoint = "/boot";

  # Partition Labels
  bootLabel = "nixboot";
  unencryptedLabel = "nixos";
  encryptedPartLabel = "nixos_encrypted";

  # Partition Filesystems
  unencryptedFSType = "ext4";
  bootFSType = "vfat";

  # Mapper Name
  mapperName = "cryptroot";

  # FS Paths
  encryptedPath = "/dev/disk/by-partlabel/${encryptedPartLabel}";
  decryptedPath = "/dev/mapper/${mapperName}";

  # the /boot parition
  bootPath = "/dev/disk/by-label/${bootLabel}";
}
