rec {
  restic = let
    OnStartupSec = "1h";
  in {
    low = {
      inherit OnStartupSec;
      OnUnitActiveSec = "48h";
    };
    medium = {
      inherit OnStartupSec;
      OnUnitActiveSec = "16h";
    };
    high = {
      inherit OnStartupSec;
      OnUnitActiveSec = "6h";
    };
  };

  remoteBackups = {
    OnStartupSec = "1h";
    OnUnitActiveSec = "8h";
  };
}
