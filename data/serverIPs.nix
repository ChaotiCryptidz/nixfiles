rec {
  "hetzner-arm" = {
    ipv4 = "65.21.145.62";
    ipv6 = "2a01:4f9:c012:9dbf::1";
  };

  "raspberry" = {
    ipv4 = "77.104.180.70";
    ipv6 = [
      "fd00::2ecf:67ff:fe74:940d"
      "2a02:8012:7883:0:2ecf:67ff:fe74:940d"
    ];
  };
}
