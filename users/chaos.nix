{...}: {
  users.users.chaos = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "disk"
      "video"
      "systemd-journal"
      "plugdev"
      "vfio"
      "input"
      "uinput"
      "audio"
      "rtkit"
      "i2c"
      "kvm"
      "usbmux"
      "dialout"
    ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEZpvkllLt7HinNpisOx7hWT2br68UoCg0sXKTxHEeUB chaos@chaos"
    ];
  };
}
