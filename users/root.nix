{...}: {
  users.users.root = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEZpvkllLt7HinNpisOx7hWT2br68UoCg0sXKTxHEeUB chaos@chaos"
    ];
  };
}
