{...}: {
  users.users.guest = {
    uid = 1001;
    isNormalUser = true;
    extraGroups = [
      "video"
      "input"
      "uinput"
      "audio"
      "rtkit"
      "usbmux"
    ];
  };
}
