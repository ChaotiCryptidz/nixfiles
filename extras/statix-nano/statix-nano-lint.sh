#!/usr/bin/env bash

set -uxe

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ ! -z "${DEBUG:-}" ]; then 
    JQ_ARGS="--debug-trace"
else 
    JQ_ARGS=""
fi


statix check -o json "$@" | jq ${JQ_ARGS:-} -rnesM -L $SCRIPT_DIR -L "${SCRIPT_LIB_DIR:-}" 'include "statix-to-nano-linter"; main'

