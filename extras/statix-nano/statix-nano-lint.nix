{pkgs, ...}: {
  script = pkgs.writeShellScript "nano-lint-nix.sh" ''
    #!/usr/bin/env bash
    set -uxeo pipefail

    env SCRIPT_LIB_DIR=${builtins.path {
      path = ./.;
      filter = path: _type:
        path == toString ./statix-to-nano-linter.jq;
    }} bash ${./statix-nano-lint.sh} $@
  '';
}
