# severity_or_unknown tests 
## All Valid Severities
include "statix-to-nano-linter"; . | severity_or_unknown
"Warn"
"Warn"

include "statix-to-nano-linter"; severity_or_unknown
"Error"
"Error"

include "statix-to-nano-linter"; severity_or_unknown
"Hint"
"Hint"

## Invalid but used elsewhere in other code
include "statix-to-nano-linter"; severity_or_unknown
"Info"
"Unknown"

include "statix-to-nano-linter"; severity_or_unknown
"Advice"
"Unknown"

## Fake
include "statix-to-nano-linter"; severity_or_unknown
"beep"
"Unknown"

## Technically True
include "statix-to-nano-linter"; severity_or_unknown
"Unknown"
"Unknown"


# severity_to_inital tests 
# Valid
include "statix-to-nano-linter"; severity_to_inital
"Warn"
"W"