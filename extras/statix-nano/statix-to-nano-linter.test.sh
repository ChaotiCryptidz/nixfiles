#!/usr/bin/env bash

set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

jq -L $SCRIPT_DIR --run-tests $SCRIPT_DIR/statix-to-nano-linter.test.jq