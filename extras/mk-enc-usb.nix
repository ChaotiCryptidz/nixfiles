{
  parted,
  cryptsetup,
  e2fsprogs,
  writeShellApplication,
}: let
  encryptedUSBData = import ../data/drives/encryptedUSB.nix;
in
  writeShellApplication {
    name = "mk-enc-usb";
    runtimeInputs = [
      parted
      cryptsetup
      e2fsprogs
    ];
    text = ''
      if [ -z "''${1-}" ]; then
        echo "Please specify a path to device as first argument"
        exit 1
      fi

      # e.g /dev/sdb
      USB_DEVICE=$1

      if [ "$EUID" -ne 0 ]; then
        echo "Please run as root"
        exit
      fi

      echo "Creating Encrypted USB."

      echo "Creating Partitions..."
      parted "$USB_DEVICE" -- mklabel gpt
      parted "$USB_DEVICE" -- mkpart primary 0% 100%

      echo "Creating Encrypted Partition"
      cryptsetup luksFormat "''${USB_DEVICE}1"

      echo "Opening Encrypted Partition"
      cryptsetup open "''${USB_DEVICE}1" "mk_enc_usb"

      echo "Making Encrypted Filesystem"
      mkfs.ext4 -L "${encryptedUSBData.unencryptedLabel}" /dev/mapper/mk_enc_usb

      echo "Closing Encrypted Partition"
      cryptsetup close "mk_enc_usb"

      # Do this now so that i can run the damn script with usb-automount and stop it trying to mount
      echo "Naming Partitions"
      parted "$USB_DEVICE" -- name 1 ${encryptedUSBData.encryptedPartLabel}
    '';
  }
