{
  pkgs,
  lib,
  config,
  ...
}: let
  inherit (lib.strings) concatStringsSep;
  inherit (lib.lists) forEach;

  inherit (builtins) attrNames;

  inherit (pkgs) writeShellScriptBin;

  containerNames = attrNames config.containers;

  vaccumSize = "50M";
in {
  environment.systemPackages =
    [
      (writeShellScriptBin "server-extras-info" ''
        ${pkgs.bat}/bin/bat -l markdown ${builtins.toFile "server-extras-info.md" ''
          # Available Commands:
          - journalctl-vaccum-all
              Vaccums host and all container systemd journals
          - journalctl-vaccum-host
              Vaccums systemd journal on host
          - journalctl-vaccum-`$name`
              Vaccums systemd journal on a specific container
          - journalctl-container-`$name`
              journalctl but for a specific container
          - systemctl-container-`$name`
              systemctl but for a specific container
          - systemctl-list-failed-all
              Lists all failed units in host and containers
          - restart-service-all
              Restarts a service on host and all containers
          - run-command-all
              Runs a command on host and all containers
          - shell-enter-`$name`
              Opens an interactive shell with container
        ''}
      '')
      (writeShellScriptBin "journalctl-vaccum-all" ''
        journalctl --vacuum-size=${vaccumSize}
        ${concatStringsSep "\n" (forEach containerNames (name: ''
          journalctl --vacuum-size=${vaccumSize} --root /var/lib/nixos-containers/${name}
        ''))}
      '')
      (writeShellScriptBin "journalctl-vaccum-host" ''
        journalctl --vacuum-size=${vaccumSize}
      '')
      (writeShellScriptBin "systemctl-list-failed-all" ''
        echo "Host: "
        systemctl --failed
        ${concatStringsSep "\n" (forEach containerNames (name: ''
          echo "Container: ${name}"
          systemctl -M ${name} --failed
        ''))}
      '')
      (writeShellScriptBin "restart-service-all" ''
        echo "Host: "
        systemctl restart $@
        ${concatStringsSep "\n" (forEach containerNames (name: ''
          echo "Container: ${name}"
          systemctl -M ${name} restart $@
        ''))}
      '')
      (writeShellScriptBin "run-command-all" ''
        echo "Host: "
        $@
        ${concatStringsSep "\n" (forEach containerNames (name: ''
          echo "Container: ${name}"
          machinectl shell ${name} $@
        ''))}
      '')
    ]
    ++ forEach containerNames (name: (writeShellScriptBin "journalctl-vaccum-${name}" ''
      journalctl --vacuum-size=${vaccumSize} --root /var/lib/nixos-containers/${name}
    ''))
    ++ forEach containerNames (name: (writeShellScriptBin "systemctl-container-${name}" ''
      systemctl -M ${name} "$@"
    ''))
    ++ forEach containerNames (name: (writeShellScriptBin "journalctl-container-${name}" ''
      journalctl -M ${name} "$@"
    ''))
    ++ forEach containerNames (name: (writeShellScriptBin "shell-enter-${name}" ''
      machinectl shell ${name}
    ''));
}
