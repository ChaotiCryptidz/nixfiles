{lib, ...}: let
  inherit (lib.modules) mkForce;
in {
  networking = {
    networkmanager = {
      enable = true;
      connectionConfig = {"ipv6.ip6-privacy" = mkForce 1;};
    };
  };
}
