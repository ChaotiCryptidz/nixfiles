{
  pkgs,
  lib,
  ...
}: let
  inherit (lib.modules) mkForce;
in {
  boot.binfmt = {
    emulatedSystems = ["x86_64-linux"];
    registrations.x86_64-linux = {
      interpreter = mkForce "${pkgs.qemu}/bin/qemu-x86_64";
    };
  };
  nix.settings.extra-sandbox-paths = ["/run/binfmt" "${pkgs.qemu}"];
}
