{
  pkgs,
  lib,
  ...
}: let
  inherit (lib.modules) mkForce;
in {
  boot.binfmt = {
    emulatedSystems = ["aarch64-linux"];
    registrations.aarch64-linux = {
      interpreter = mkForce "${pkgs.qemu}/bin/qemu-aarch64";
      fixBinary = true;
    };
  };
  nix.settings.extra-sandbox-paths = ["/run/binfmt" "${pkgs.qemu}"];
}
