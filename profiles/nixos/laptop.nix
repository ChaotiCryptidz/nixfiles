{config, ...}: {
  services.libinput.enable = true;
  powerManagement.enable = true;
  hardware.acpilight.enable = true;
  environment.systemPackages = [config.boot.kernelPackages.cpupower];
}
