{pkgs, ...}: {
  services.tor = {
    enable = true;
    client = {
      enable = true;
      dns.enable = true;
    };
    torsocks.enable = true;
    settings = {
      UseBridges = true;
      ClientTransportPlugin = [
        "meek_lite,obfs2,obfs3,obfs4,scramblesuit,webtunnel exec ${pkgs.obfs4}/bin/lyrebird"
        "snowflake exec ${pkgs.snowflake}/bin/client"
      ];
      Bridge = [
        "obfs4 45.14.244.129:9999 00249AC9247EB4F3271340F5D0BC470F761CC5C2 cert=SUrKlv/yozKJ9S8x4yHbdk08DfFYYHFMdwDBTmybeTubXAy9xy2JtqAZ1diDikRyKGgfPw iat-mode=0"
        "obfs4 185.177.207.132:8443 4FB781F7A9DD39DA53A7996907817FC479874D19 cert=UL2gCAXWW5kEWY4TQ0lNeu6OAmzh40bXYVhMnTWVG8USnyy/zEKGSIPgmwTDMumWr9c1Pg iat-mode=0"

        "meek_lite 192.0.2.18:80 BE776A53492E1E044A26F17306E1BC46A55A1625 url=https://meek.azureedge.net/ front=ajax.aspnetcdn.com"

        "snowflake 192.0.2.3:80 2B280B23E1107BB62ABFC40DDCC8824814F80A72 fingerprint=2B280B23E1107BB62ABFC40DDCC8824814F80A72 url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.com:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn"
        "snowflake 192.0.2.4:80 8838024498816A039FCBBAB14E6F40A0843051FA fingerprint=8838024498816A039FCBBAB14E6F40A0843051FA url=https://1098762253.rsc.cdn77.org/ fronts=www.cdn77.com,www.phpmyadmin.net ice=stun:stun.l.google.com:19302,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.net:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 utls-imitate=hellorandomizedalpn"

        "webtunnel [2001:db8:234d:72d0:9dca:d0d5:47ee:3e88]:443 E9EC35F8ABF781B05FF95F932BBC8EFC8F5E3700 url=https://skyblond.info/qN5l8Zpr9h8hBjFM4cxukw48 ver=0.0.1"
      ];
    };
  };
}
