{
  lib,
  config,
  ...
}: let
  inherit (lib.modules) mkIf mkMerge;

  currentHostname = config.networking.hostName;

  usbSSHKeyFile = "/usb/ssh-keys/chaos.priv";
  normalSSHKeyFile = "/home/chaos/.ssh/id_ed25519";

  sshKeyFile =
    if
      builtins.elem currentHostname [
        "lappy-t495"
      ]
    then usbSSHKeyFile
    else if
      builtins.elem currentHostname [
      ]
    then normalSSHKeyFile
    else throw "host isn't configured for remote-builders";

  builderDefaults = {
    sshUser = "root";
    sshKey = sshKeyFile;
    supportedFeatures = ["nixos-test" "benchmark" "big-parallel" "kvm"];
    mandatoryFeatures = [];
  };
in {
  nix.buildMachines = [
    (mkIf (currentHostname != "hetzner-arm") (mkMerge [
      builderDefaults
      {
        hostName = "hetzner-arm.servers.genderfucked.monster";
        systems = ["aarch64-linux"];
        supportedFeatures = ["native-arm64" "big-parallel"];
        publicHostKey = "c3NoLWVkMjU1MTkgQUFBQUMzTnphQzFsWkRJMU5URTVBQUFBSUk5cGM0REU1UlV4UUp2T1pwenFOQWVac0JlRW1kcmp4OFlnV3orVXBMckcgcm9vdEBoZXR6bmVyLWFybQo=";
        maxJobs = 3;
        speedFactor = 1;
      }
    ]))
    (mkIf (currentHostname != "raspberry-pi5") (mkMerge [
      builderDefaults
      {
        hostName = "raspberry-pi5.servers.genderfucked.monster";
        systems = ["aarch64-linux"];
        supportedFeatures = ["native-arm64" "big-parallel"];
        publicHostKey = "c3NoLWVkMjU1MTkgQUFBQUMzTnphQzFsWkRJMU5URTVBQUFBSUNZNFpuQ1NVeWpjOHR3UXVpMzhwU21qdHluSVZUVnFiNzB5blNRa0Z3anYgcm9vdEByYXNwYmVycnkK";
        maxJobs = 4;
        speedFactor = 4;
      }
    ]))
  ];
  nix.distributedBuilds = true;
  nix.extraOptions = "builders-use-substitutes = true";
}
