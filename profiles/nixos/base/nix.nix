{
  self,
  inputs,
  config,
  pkgs,
  lib,
  ...
}: let
  inherit (lib.strings) optionalString versionAtLeast;
  inherit (lib.lists) optional;
  inherit (lib.modules) mkIf;
in {
  environment.systemPackages = with pkgs; [nix-tree nix-output-monitor];

  nix = {
    nixPath = ["nixpkgs=${inputs.nixpkgs}"];
    extraOptions =
      optionalString
      (versionAtLeast config.nix.package.version "2.4") ''
        experimental-features = nix-command flakes
      '';
    settings.system-features = ["big-parallel"] ++ lib.optional (pkgs.system == "aarch64-linux") "native-arm64";
    settings.trusted-users = ["root" "@wheel"];
  };
  nixpkgs = mkIf (!config.boot.isContainer) {
    config = {
      allowUnfree = true;
    };
    overlays = [
      (import "${self}/overlay")
    ];
  };
}
