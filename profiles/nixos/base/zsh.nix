{pkgs, ...}: {
  users.defaultUserShell = pkgs.zsh;
  environment.pathsToLink = ["/share/zsh"];
  environment.shells = with pkgs; [zsh];
  programs.zsh.enable = true;
}
