{
  lib,
  pkgs,
  config,
  ...
}: let
  inherit (lib.modules) mkIf;

  is_x86 = "${pkgs.system}" == "x86_64-linux";
in {
  hardware = {
    enableAllFirmware = false; # we include a more minimal subset for base
    enableRedistributableFirmware = true;
    wirelessRegulatoryDatabase = true;

    firmware = with pkgs; [
      linux-firmware
    ];

    ksm.enable = mkIf (!config.boot.isContainer) true;

    i2c.enable = true;

    cpu = {
      intel.updateMicrocode = mkIf is_x86 true;
      amd.updateMicrocode = mkIf is_x86 true;
    };
  };
}
