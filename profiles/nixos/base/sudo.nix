{lib, ...}: let
  inherit (lib.modules) mkDefault;
in {
  security.sudo.wheelNeedsPassword = mkDefault false;
}
