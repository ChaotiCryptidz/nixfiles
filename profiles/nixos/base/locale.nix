{lib, ...}: let
  inherit (lib.modules) mkDefault;
in {
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    earlySetup = true;
    font = mkDefault "Lat2-Terminus16";
    keyMap = "uk";
  };
}
