{
  self,
  inputs,
  tree,
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkIf;
  inherit (lib.lists) flatten forEach;
  inherit (lib.options) mkOption;
  inherit (lib.types) attrsOf submoduleWith;
in {
  options.home-manager.users = mkOption {
    type = attrsOf (submoduleWith {
      modules = [];
      specialArgs = {
        inherit inputs tree self;
      };
    });
  };

  config = {
    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;
      sharedModules = with tree; [];
    };
    systemd.tmpfiles.rules = mkIf config.boot.isContainer (flatten (forEach (builtins.attrNames config.home-manager.users) (user: [
      "d /nix/var/nix/profiles/per-user/${user} - ${config.users.users."${user}".group} - - -"
      "d /nix/var/nix/gcroots/per-user/${user} - ${config.users.users."${user}".group} - - -"
    ])));
  };
}
