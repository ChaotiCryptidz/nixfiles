{
  pkgs,
  lib,
  ...
}: let
  inherit (lib.options) mkOption;
  inherit (lib.modules) mkDefault;
  inherit (lib.types) submodule attrsOf;
in {
  options = {
    services.nginx.virtualHosts = mkOption {
      type = attrsOf (submodule {
        config.http3 = mkDefault true;
        config.http3_hq = mkDefault true;
        config.quic = mkDefault true;
      });
    };
  };

  config = {
    security.acme = {
      defaults = {
        email = "chaoticryptidz@owo.monster";
      };
      acceptTerms = true;
    };

    services.nginx = {
      enable = true;
      package = pkgs.nginxQuic;
      recommendedGzipSettings = true;
      recommendedBrotliSettings = true;
      recommendedZstdSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      clientMaxBodySize = mkDefault "512m";
      serverNamesHashBucketSize = 1024;
      appendHttpConfig = ''
        proxy_headers_hash_max_size 1024;
        proxy_headers_hash_bucket_size 256;
      '';
    };

    services.logrotate.settings.nginx = {
      minsize = "50M";
      rotate = "4"; # 4 files of 50mb each
      compress = "";
    };

    services.logrotate.settings.nginx.enable = true;
  };
}
