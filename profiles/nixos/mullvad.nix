{config, ...}: {
  services.mullvad-vpn.enable = true;
  networking.resolvconf.enable = true;
  systemd.services.mullvad-daemon.path = [
    config.networking.resolvconf.package
  ];
}
