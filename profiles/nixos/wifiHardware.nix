{
  lib,
  pkgs,
  ...
}: let
  inherit (lib.lists) optional;
in {
  hardware = {
    firmware = with pkgs;
      [
        wireless-regdb

        # Realtek
        rtl8192su-firmware
        rt5677-firmware
        rtl8761b-firmware
        # ZyDAS
        zd1211fw
        # Broadcom
        b43Firmware_5_1_138
        b43Firmware_6_30_163_46
      ]
      ++ optional pkgs.stdenv.hostPlatform.isAarch raspberrypiWirelessFirmware;
  };
}
