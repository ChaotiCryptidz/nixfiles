{
  pkgs,
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkForce;
in {
  boot.kernelPackages = mkForce pkgs.linuxPackages_latest;
  environment.systemPackages = [config.boot.kernelPackages.cpupower];
}
