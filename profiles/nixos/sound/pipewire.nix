{...}: {
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    # TODO: fix config
    #config.pipewire = {
    #  "context.properties" = {
    #    # So fiio btr3k works,
    #    "default.clock.rate" = "48000";
    #  };
    #};
    wireplumber.enable = true;
  };
}
