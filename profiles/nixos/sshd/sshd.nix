{lib, ...}: let
  inherit (lib.modules) mkDefault;
  inherit (lib.modules) mkOverride;
in {
  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = mkOverride 50 "prohibit-password";
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = mkDefault false;
      StreamLocalBindUnlink = true;
      KexAlgorithms = ["curve25519-sha256@libssh.org"];
      LogLevel = "VERBOSE";
    };
  };
}
