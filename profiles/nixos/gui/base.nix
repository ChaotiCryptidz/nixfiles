{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    adwaita-icon-theme
  ];

  services.udev.extraRules = ''
    KERNEL=="uinput", SUBSYSTEM=="misc", TAG+="uaccess", OPTIONS+="static_node=uinput", GROUP="input", MODE="0660"
  '';

  hardware.steam-hardware.enable = true;

  programs.dconf.enable = true;

  services.xserver.xkb = {
    layout = "gb";
  };

  fonts = {
    packages = with pkgs; [comic-sans comic-code];
    fontconfig = {
      # For Telegram
      localConf = ''
        <?xml version="1.0"?>
        <!DOCTYPE fontconfig SYSTEM "urn:fontconfig:fonts.dtd">
        <fontconfig>

          <match>
           <test name="family"><string>Open Sans</string></test>
           <edit name="family" mode="assign" binding="strong">
            <string>Comic Sans MS</string>
           </edit>
          </match>
          <match>
           <test name="family"><string>Open Sans Semibold</string></test>
           <edit name="family" mode="assign" binding="strong">
            <string>Comic Sans MS:style=bold</string>
           </edit>
          </match>
        </fontconfig>
      '';
      defaultFonts = {
        serif = ["Comic Sans MS"];
        sansSerif = ["Comic Sans MS"];
        monospace = ["Comic Code"];
      };
    };
  };
}
