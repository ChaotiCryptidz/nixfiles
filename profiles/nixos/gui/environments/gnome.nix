{
  pkgs,
  lib,
  ...
}: let
  inherit (lib.modules) mkIf mkForce;

  isWayland = true;
in {
  services.xserver = {
    enable = true;
    desktopManager.gnome.enable = true;
    displayManager.gdm = {
      enable = true;
      wayland = isWayland;
      autoSuspend = false;
    };
  };

  environment.variables = {
    QT_QPA_PLATFORM =
      if isWayland
      then "wayland-egl"
      else "xcb";
    SDL_VIDEODRIVER =
      if isWayland
      then "wayland,x11,windows"
      else "x11,windows";
    _JAVA_AWT_WM_NONREPARENTING = mkIf isWayland "1";
  };

  security.polkit.extraConfig = ''
    polkit.addRule(function(action, subject) {
      if (subject.isInGroup("wheel")) {
        return polkit.Result.YES;
      }
    });
  '';

  environment.gnome.excludePackages = with pkgs; [
    cheese
    gnome-music
    gnome-terminal
    seahorse
    gedit
    epiphany
    evince
    gnome-characters
    totem
    tali
    iagno
    hitori
    atomix
    gnome-tour
    geary
  ];

  environment.systemPackages = with pkgs; [
    gnome-tweaks
    gnomeExtensions.appindicator
    gnomeExtensions.just-perfection

    qt5.qtwayland
    wl-clipboard
    xclip
  ];

  services.gnome.gnome-browser-connector.enable = true;
  services.udev.packages = with pkgs; [gnome.gnome-settings-daemon];

  services.power-profiles-daemon.enable = true;
  hardware.pulseaudio.enable = mkForce false;

  programs.dconf.enable = true;

  services.gnome.gnome-keyring.enable = mkForce false;
}
