{pkgs, ...}: {
  home.packages = with pkgs; [
    binwalk
    file
    binutils
    diffoscopeMinimal
  ];
}
