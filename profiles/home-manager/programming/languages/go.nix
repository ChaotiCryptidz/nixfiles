{pkgs, ...}: {
  home.packages = with pkgs; [go gopls go-outline gotools];
  programs.vscode.extensions = with pkgs; [vscode-extensions.golang.go];
}
