{pkgs, ...}: {
  programs.vscode.extensions = with pkgs; [vscode-extensions.rust-lang.rust-analyzer];
  home.packages = with pkgs; [rustc cargo clippy rust-analyzer rustfmt];
  home.sessionVariables = {RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;};
}
