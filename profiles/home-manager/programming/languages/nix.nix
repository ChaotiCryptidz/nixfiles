{pkgs, ...}: {
  home.packages = with pkgs; [alejandra deadnix statix];

  programs.vscode.extensions = with pkgs; [
    vscode-extensions.bbenoist.nix
    vscode-extensions.kamadorueda.alejandra
  ];

  programs.vscode.userSettings."[nix]" = {
    "editor.defaultFormatter" = "kamadorueda.alejandra";
    "editor.formatOnSave" = true;
  };
  programs.vscode.userSettings = {
    "alejandra.program" = "alejandra";
  };

  programs.zsh.shellAliases.nixdirfmt = "alejandra . && statix fix . && deadnix -e .";
}
