{
  self,
  pkgs,
  config,
  ...
} @ inputs: let
  package =
    if inputs ? "nixosConfig"
    then inputs.nixosConfig.programs.nano.package
    else pkgs.nano;
in {
  home.packages = with pkgs; [package deadnix statix jq];
  systemd.user.tmpfiles.rules = [
    "d ${config.xdg.cacheHome}/nano - ${config.home.username} users"
  ];

  xdg.configFile."nano/nanorc".text = ''
    set softwrap
    set tabsize 2
    set autoindent
    set backup
    set backupdir "${config.xdg.cacheHome}/nano"
    set indicator
    set magic
    unset mouse
    set nonewlines
    set constantshow
    set positionlog
    set multibuffer
    unset minibar

    bind ^I formatter all
    bind M-I formatter all
    bind Sh-M-I formatter all

    bind ^L linter all
    bind M-L linter all
    bind Sh-M-L linter all

    include "${package}/share/nano/*.nanorc"
    include "${package}/share/nano/extra/*.nanorc"
    include "${pkgs.nanorc}/share/*.nanorc"

    extendsyntax rust formatter rustfmt
    extendsyntax nix formatter alejandra -t1
    extendsyntax nix linter ${pkgs.writeShellScript "nano-lint-nix.sh" (let
      statix-nano = import "${self}/extras/statix-nano/statix-nano-lint.nix" inputs;
    in ''
      #!/usr/bin/env bash
      set -uxeo pipefail
      ${statix-nano.script} $@
      deadnix $@ -o json | jq -r '.file as $filename | .results | .[] | $filename + ":" + (.line|tostring) + ":" + (.column|tostring) + ": " + .message'
    '')}
  '';
}
