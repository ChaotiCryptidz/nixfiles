{...}: {
  home.sessionVariables = {
    DONT_PROMPT_WSL_INSTALL = 1;
  };
  programs.vscode = {
    enable = true;
    userSettings = {
      "terminal.integrated.shellIntegration.enabled" = false;
      "github.gitAuthentication" = false;
      "editor.fontSize" = 24;
      "editor.fontFamily" = "'Comic Code'";
      "terminal.integrated.fontSize" = 18;
      "editor.codeLensFontFamily" = "'Comic Code'";
      "editor.inlayHints.fontFamily" = "'Comic Code'";
      "markdown.preview.fontFamily" = "'Comic Code'";
      "terminal.integrated.fontFamily" = "'Comic Code'";
      "files.autoSave" = "afterDelay";
      "window.zoomLevel" = 0;
      "editor.tabSize" = 2;
    };
  };
}
