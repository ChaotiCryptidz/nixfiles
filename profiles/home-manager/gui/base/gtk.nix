{pkgs, ...}: {
  gtk = {
    enable = true;
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
    theme = {
      name = "Nordic";
      package = pkgs.nordic;
    };
    font = {
      name = "Comic Code";
      size = 12;
      package = pkgs.comic-code;
    };
  };
}
