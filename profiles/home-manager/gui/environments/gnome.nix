{
  tree,
  pkgs,
  inputs,
  lib,
  ...
}: let
  homeManagerLib = inputs.home-manager.lib.hm;

  fontSizes = {
    small = "10";
    medium = "12";
  };
in {
  imports = with tree.profiles.home-manager; [gui.base apps.kitty];

  home.packages = with pkgs; [
    dconf2nix
    dconf-editor
    xclip
  ];

  home.sessionVariables = {
    SAL_USE_VCLPLUGIN = "gtk3"; # GTK3 on LibreOffice
  };

  dconf.enable = true;
  dconf.settings = {
    "org/gnome/mutter" = {dynamic-workspaces = false;};

    "org/gnome/desktop/interface" = {
      # Clock at Top Bar
      #clock-show-seconds = true;
      clock-show-weekday = true;
      # Battery Percentage on Top Bar
      show-battery-percentage = true;
      clock-format = "12h";

      # Cursor Size (only seems to affect VSCode??)
      # TODO: Fix This
      cursor-size = 32;
      # I hate animations.
      enable-animations = false;
      # Handy for when using mouse.
      enable-hot-corners = true;
      # Fonts and hinting settings.
      # TODO: Maybe do this with fontconfig too?
      font-antialiasing = "rgba";
      font-hinting = "full";
      font-name = "Comic Code ${fontSizes.medium}";
      document-font-name = "Comic Code ${fontSizes.medium}";
      monospace-font-name = "Comic Code ${fontSizes.small}";
      color-scheme = "prefer-dark";
    };
    "org/gnome/desktop/input-sources" = {
      # TODO: see if this changes when using gnome wayland?
      sources = [(homeManagerLib.gvariant.mkTuple ["xkb" "gb"])];
      per-window = false;
    };
    "org/gnome/desktop/media-handling" = {
      # growl i hate this until i sometimes dont in which case i love it
      automount = false;
    };
    "org/gnome/desktop/notifications" = {show-in-lockscreen = false;};
    "org/gnome/desktop/peripherals/mouse" = {
      # NO!
      natural-scroll = false;
    };
    "org/gnome/desktop/peripherals/touchpad" = {
      # NO!
      natural-scroll = false;
      # YES!
      two-finger-scrolling-enabled = true;
      tap-to-click = true;
    };
    "org/gnome/desktop/privacy" = {
      hide-identity = true;
      old-files-age = 5;
      recent-files-max-age = 0;
      remember-app-usage = false;
      remember-recent-files = false;
      remove-old-temp-files = true;
      remove-old-trash-files = true;
    };
    # use location services to set time
    "org/gnome/system/location" = {enabled = true;};
    "org/gnome/desktop/datetime" = {automatic-timezone = true;};
    "org/gnome/desktop/wm/keybindings" = {
      # mostly just i3wm stock keybinds
      close = ["<Shift><Alt>q"];
      maximize = ["<Shift><Alt>f"];
      toggle-fullscreen = ["<Alt>f"];

      # Workspace Switch
      switch-to-workspace-1 = ["<Alt>1"];
      switch-to-workspace-2 = ["<Alt>2"];
      switch-to-workspace-3 = ["<Alt>3"];
      switch-to-workspace-4 = ["<Alt>4"];
      switch-to-workspace-5 = ["<Alt>5"];
      switch-to-workspace-6 = ["<Alt>6"];
      switch-to-workspace-7 = ["<Alt>7"];
      switch-to-workspace-8 = ["<Alt>8"];
      switch-to-workspace-9 = ["<Alt>9"];

      # Workspace Move Window
      # TODO: Hardcoded for UK keyboards
      move-to-workspace-1 = ["<Shift><Alt>exclam"];
      move-to-workspace-2 = ["<Shift><Alt>quotedbl"];
      move-to-workspace-3 = ["<Shift><Alt>sterling"];
      move-to-workspace-4 = ["<Shift><Alt>dollar"];
      move-to-workspace-5 = ["<Shift><Alt>percent"];
      move-to-workspace-6 = ["<Shift><Alt>asciicircum"];
      move-to-workspace-7 = ["<Shift><Alt>ampersand"];
      move-to-workspace-8 = ["<Shift><Alt>asterisk"];
      move-to-workspace-9 = ["<Shift><Alt>parenleft"];
    };
    "org/gnome/desktop/wm/preferences" = {
      num-workspaces = 9;
      titlebar-font = "Comic Code Medium ${fontSizes.small}";
      titlebar-uses-system-font = true;
    };
    "org/gnome/settings-daemon/plugins/media-keys" = {
      area-screenshot = [];
      area-screenshot-clip = ["<Shift>Print"];
      screenshot = [];
      screenshot-clip = ["Print"];
      search = ["<Alt>d"];
      custom-keybindings = [
        # Kitty
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/kitty/"
      ];
    };
    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/kitty" = {
      binding = "<Alt>Return";
      command = "kitty";
      name = "kitty";
    };
    "org/gnome/shell" = {
      enabled-extensions = ["just-perfection-desktop@just-perfection"];
    };
    "org/gnome/shell/extensions/just-perfection" = {
      activities-button = true;
      animation = 0;
      dash = false;
      dash-icon-size = 0;
      panel = true;
      panel-in-overview = true;
      search = false;
      show-apps-button = true;
      show-prefs-intro = false;
      theme = false;
      workspace = true;
      workspace-popup = false;
      workspace-switcher-should-show = false;
    };
  };
}
