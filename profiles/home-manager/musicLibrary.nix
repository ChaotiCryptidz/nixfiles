{
  pkgs,
  tree,
  ...
}: {
  imports = with tree.profiles.home-manager; [
    apps.rclone
    apps.musicutil
  ];

  home.packages = [
    pkgs.nodePackages.html-minifier
  ];

  home.file."Music/music-sync-check.sh" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash

      SCRIPT_DIR=$( cd -- "$( dirname -- "''${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
      cd "''${SCRIPT_DIR}"

      ERROR_LOG=$(mktemp -t music-check-log-XXX)

      echo "Checking StorageBox sync status"
      if rclone check . Storage:Music --exclude "/*.sh" 2>$ERROR_LOG; then
        echo "Up to date with StorageBox"
      else
        echo "An error occured attempting to check sync status:"
        cat "$ERROR_LOG"
        echo
      fi

      rm "$ERROR_LOG"
    '';
  };

  home.file."Music/music-gen-listing.sh" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash

      TMPDIR=$(mktemp -d)

      TITLE="chaos's Music Library"
      DESCRIPTION="A listing of all music we listen to and have downloaded/brought"
      LINK_BASE="https://storage-http.owo.monster/Music"

      musicutil genhtml . "$TMPDIR" --title "$TITLE" --description "$DESCRIPTION" --link-base="$LINK_BASE"

      pushd "$TMPDIR"

      html-minifier \
        --collapse-whitespace --remove-comments \
        --remove-redundant-attributes --remove-script-type-attributes \
        --remove-tag-whitespace --use-short-doctype \
          index.html > index_min.html
      mv index_min.html index.html

      rsync --recursive ./ hetzner-arm:/var/lib/static-sites/music_library/
      ssh hetzner-arm chown -R nginx:nginx /var/lib/static-sites/music_library

      popd
    '';
  };

  home.file."Music/music-sync.sh" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash

      SCRIPT_DIR=$( cd -- "$( dirname -- "''${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
      cd "''${SCRIPT_DIR}"

      rclone sync -P . Storage:Music --exclude "/*.sh"

      bash $HOME/Music/music-gen-listing.sh
    '';
  };

  home.file."Music/music-download.sh" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash

      SCRIPT_DIR=$( cd -- "$( dirname -- "''${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
      cd "''${SCRIPT_DIR}"

      rclone sync -P --exclude music-sync.sh,music-download.sh Storage:Music .
    '';
  };
}
