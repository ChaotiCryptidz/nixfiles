{tree, ...}: {
  # basically everything apart from dev.debugging and extra archives
  imports = with tree.profiles.home-manager.dev.all; [
    archives.common
    compression
    editors
    extra
    git
    info
    network
  ];
}
