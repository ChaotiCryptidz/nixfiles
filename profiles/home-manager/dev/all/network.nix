{
  pkgs,
  lib,
  ...
} @ inputs: let
  inherit (lib.modules) mkIf;
in {
  home.packages = with pkgs; [
    curl
    wget
    dig
    whois
    dnsutils
    rsync
    openssh
    nmap
    tcpdump
    iftop
    speedtest-cli
    (mkIf (!(inputs ? nixosConfig)) mtr)
  ];
}
