{pkgs, ...}: {
  home.packages = with pkgs; [
    zstd
    zlib
    xz
    gzip
    bzip2
    cpio
    lz4
  ];
}
