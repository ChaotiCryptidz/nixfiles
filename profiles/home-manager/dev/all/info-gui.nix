{pkgs, ...}: {
  home.packages = with pkgs;
    [
      wmctrl
      ddcutil
      glxinfo

      libva-utils
      vdpauinfo
      clinfo
    ]
    ++ (with pkgs.xorg; [
      xdpyinfo
      xprop
      xrandr
    ]);
}
