{pkgs, ...}: {
  home.packages = with pkgs; [
    jq
    ripgrep
    fd
    pv
    tmux
    socat
    file
    python3
    binutils # for strings
    qrencode
    dos2unix

    # This saves a rebuild of already cached busybox
    (pkgs.runCommand "busybox-no-applets" {} ''
      mkdir -p $out/bin
      ln -s ${pkgs.busybox}/bin/busybox $out/bin/busybox
    '')

    mk-enc-usb
    mk-encrypted-drive
  ];
}
