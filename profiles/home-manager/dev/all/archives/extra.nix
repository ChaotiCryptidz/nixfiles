{pkgs, ...}: {
  home.packages = with pkgs; [
    cabextract
    unshield
    squashfsTools
    cpio
    lz4
  ];
}
