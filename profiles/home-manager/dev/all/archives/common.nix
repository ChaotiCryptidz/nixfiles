{pkgs, ...}: {
  home.packages = with pkgs; [
    libarchive
    zip
    p7zip
  ];

  programs.zsh.shellAliases.tar = "bsdtar";
}
