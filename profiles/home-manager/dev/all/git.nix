{
  pkgs,
  lib,
  config,
  ...
}: let
  inherit (lib.modules) mkDefault;
in {
  programs.git = {
    enable = true;
    lfs.enable = true;
    package = mkDefault pkgs.gitMinimal;
    userName = "chaos";
    userEmail = "chaos@owo.monster";
    extraConfig = {
      credential.helper = "store";
    };
  };

  home.packages = [
    (pkgs.runCommand "git-extras" {} (let
      gitLibExec = "${config.programs.git.package}/libexec/git-core";
    in ''
      mkdir -p $out/bin
      ln -s ${gitLibExec}/git-diff $out/bin/git-diff
    ''))
  ];
}
