{tree, ...}: {
  imports = with tree.profiles.home-manager; [
    programming.editors.nano
  ];
}
