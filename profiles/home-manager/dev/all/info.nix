{pkgs, ...}: {
  home.packages = with pkgs; [
    neofetch
    (inxi.override {
      withRecommendedSystemPrograms = true;
    })
    lm_sensors
    hddtemp
    freeipmi
    ipmitool
    smartmontools
    tree
    lsof
    btop
    htop
    pciutils
    usbutils
    i2c-tools
    iotop
    iptraf-ng
  ];
}
