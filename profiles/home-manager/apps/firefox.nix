{
  inputs,
  nixosConfig,
  pkgs,
  ...
}: let
  nur = import inputs.nur {
    nurpkgs = pkgs;
    inherit pkgs;
  };

  isGnome = nixosConfig.services.xserver.desktopManager.gnome.enable;

  extensions = with nur.repos.rycee.firefox-addons; [
    ublock-origin
    stylus
    tampermonkey
    search-engines-helper
    search-by-image
    offline-qr-code-generator
    i-dont-care-about-cookies
    don-t-fuck-with-paste
    amp2html
    a11ycss
  ];
in {
  programs.firefox = {
    enable = true;
    package = pkgs.firefox.override {
      cfg = {
        # for gnome media shenanigans
        enableGnomeExtensions = isGnome;
      };
    };
    profiles."profile" = {
      name = "profile";
      id = 0;
      isDefault = true;
      inherit extensions;
      settings = {
        "app.normandy.first_run" = false;
        "browser.search.region" = "GB";
        "intl.locale.requested" = "en-GB,en-US";

        # theme
        "layout.css.prefers-color-scheme.content-override" = "0";

        # less junk on homescreen
        "extensions.htmlaboutaddons.recommendations.enabled" = false;
        "browser.newtabpage.activity-stream.feeds.recommendationprovider" =
          false;
        "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
        "browser.newtabpage.activity-stream.feeds.topsites" = false;
        "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" =
          false;
        "browser.newtabpage.activity-stream.section.highlights.includeDownloads" =
          false;
        "browser.newtabpage.activity-stream.section.highlights.includePocket" =
          false;
        "browser.newtabpage.activity-stream.section.highlights.includeVisited" =
          false;
        "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" =
          false;
        "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" =
          false;

        # dont use any form autofill
        "signon.autofillForms" = false;
        "signon.generation.enabled" = false;
        "signon.management.page.breach-alerts.enabled" = false;
        "signon.rememberSignons" = false;
        "dom.forms.autocomplete.formautofill" = false;
        "extensions.formautofill.creditCards.enabled" = false;
        "browser.formfill.enable" = false;

        # disable search suggestions
        "browser.search.suggest.enabled" = false;

        # no session store
        "browser.sessionstore.resume_from_crash" = false;

        # no telemetry
        "toolkit.telemetry.enabled" = false;
        "datareporting.healthreport.uploadEnabled" = false;
        "app.shield.optoutstudies.enabled" = false;

        # disable pocket
        "browser.pocket.enabled" = false;
        "extensions.pocket.enabled" = false;

        # no safebrowsing
        "browser.safebrowsing.enabled " = false;
        "browser.safebrowsing.phishing.enabled" = false;
        "browser.safebrowsing.malware.enabled" = false;
        "browser.safebrowsing.downloads.enabled" = false;

        # don't let websites replace right click
        "dom.event.contextmenu.enabled" = false;

        # disable geoip
        "geo.enabled" = false;
        "geo.wifi.uri" = "";
        "browser.search.geoip.url" = "";

        # enable widevine
        "media.eme.enabled" = true;
        "media.gmp-widevinecdm.enabled" = true;

        # browser toolbar and UI
        # may need updating when extensions change
        "browser.toolbars.bookmarks.visibility" = "always";
        "layout.css.devPixelsPerPx" = "0.75";
        "browser.uiCustomization.state" = builtins.toJSON {
          currentVersion = 18;
          dirtyAreaCache = [
            "nav-bar"
            "PersonalToolbar"
            "toolbar-menubar"
            "TabsToolbar"
            "widget-overflow-fixed-list"
          ];
          newElementCount = 22;
          placements = {
            PersonalToolbar = ["import-button" "personal-bookmarks"];
            TabsToolbar = ["tabbrowser-tabs" "new-tab-button" "alltabs-button"];
            nav-bar = [
              "back-button"
              "forward-button"
              "stop-reload-button"
              "urlbar-container"
              "downloads-button"
              "ublock0_raymondhill_net-browser-action"
              "firefox_tampermonkey_net-browser-action"
              "_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action"
              "offline-qr-code_rugk_github_io-browser-action"
            ];
            toolbar-menubar = ["menubar-items"];
            widget-overflow-fixed-list = [
              "a11y_css_ffoodd-browser-action"
              "dontfuckwithpaste_raim_ist-browser-action"
              "jid1-kkzogwgsw3ao4q_jetpack-browser-action"
              "_65a2d764-7358-455b-930d-5afa86fb5ed0_-browser-action"
              "_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action"
            ];
          };
          seen = [
            "save-to-pocket-button"
            "developer-button"
            "a11y_css_ffoodd-browser-action"
            "dontfuckwithpaste_raim_ist-browser-action"
            "jid1-kkzogwgsw3ao4q_jetpack-browser-action"
            "offline-qr-code_rugk_github_io-browser-action"
            "ublock0_raymondhill_net-browser-action"
            "firefox_tampermonkey_net-browser-action"
            "_65a2d764-7358-455b-930d-5afa86fb5ed0_-browser-action"
            "_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action"
            "_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action"
          ];
        };

        # Tracking, Privacy, Security
        "privacy.donottrackheader.enabled" = true;
        "privacy.globalprivacycontrol.enabled" = true;
        "browser.contentblocking.category" = "strict";
        "privacy.resistFingerprinting" = true;
        "privacy.query_stripping.enabled" = true;
        "network.cookie.sameSite.noneRequiresSecure" = true;
        "browser.uitour.enabled" = true;
        "security.ssl.treat_unsafe_negotiation_as_broken" = true;
        "browser.xul.error_pages.expert_bad_cert" = true;
        "security.tls.enable_0rtt_data" = false;
        "browser.privatebrowsing.forceMediaMemoryCache" = true;
        "browser.urlbar.trimHttps" = true;
        "security.insecure_connection_text.enabled" = true;
        "security.insecure_connection_text.pbmode.enabled" = true;
        "browser.urlbar.quicksuggest.enabled" = false;
        "browser.urlbar.suggest.quicksuggest.sponsored" = false;
        "browser.urlbar.suggest.quicksuggest.nonsponsored" = false;
        "browser.urlbar.groupLabels.enabled" = false;
        "dom.security.https_first" = true;
        "dom.security.https_only_mode_error_page_user_suggestions" = true;

        # Performance
        "content.notify.interval" = 100000;
        "gfx.canvas.accelerated.cache-items" = 4096;
        "gfx.canvas.accelerated.cache-size" = 512;
        "gfx.content.skia-font-cache-size" = 20;
        "browser.cache.jsbc_compression_level" = 3;
        "media.cache_readahead_limit" = 7200;
        "media.cache_resume_threshold" = 3600;
        "image.mem.decode_bytes_at_a_time" = 32768;
        "network.http.max-connections" = 1800;
        "network.http.max-persistent-connections-per-server" = 10;
        "network.http.max-urgent-start-excessive-connections-per-host" = 5;
        "network.dnsCacheExpiration" = 3600;
        "network.ssl_tokens_cache_capacity" = 10240;
        "network.dns.disablePrefetch" = true;
        "network.dns.disablePrefetchFromHTTPS" = true;
        "network.prefetch-next" = true;
        "network.predictor.enabled" = true;

        "layout.css.grid-template-masonry-value.enabled" = true;
        "dom.enable_web_task_scheduling" = true;
        "dom.security.sanitizer.enabled" = true;

        # RAM
        "browser.cache.memory.enable" = false;
        "media.memory_cache_max_size" = 8192;
      };
    };
  };
}
