{
  inputs,
  pkgs,
  ...
}: {
  home.packages = [
    inputs.musicutil.packages.${pkgs.system}.musicutil
  ];
}
