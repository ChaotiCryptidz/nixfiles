{pkgs, ...}: let
  listen-password-file =
    if pkgs.stdenv.isLinux
    then "/secrets/music_stream_password"
    else "$HOME/.secrets/music_stream_password";
in {
  home.packages = with pkgs; [ffmpeg yt-dlp];
  programs.mpv = {
    enable = true;
    scripts = with pkgs.mpvScripts; [mpris];
    config = {
      script-opts = "ytdl_hook-ytdl_path=${pkgs.yt-dlp}/bin/yt-dlp";
      slang = "en";
      track-auto-selection = true;
      embeddedfonts = false;
      sub-ass-force-style = "FontName=Comic Sans MS";
      sub-ass-override = "strip";
    };
    profiles = {
      ontop = {
        ontop = true;
        on-all-workspaces = true;
        window-scale = 0.5;
        cursor-autohide = 3;
      };
    };
  };
  programs.zsh.shellAliases.mpv-ontop = "mpv --profile=ontop";
  programs.zsh.shellAliases.listen = ''
    mpv "https://music:$(cat ${listen-password-file})@mpd.owo.monster/flac" --cache=yes --cache-pause-initial=yes --cache-pause-wait=5'';
}
