{...}: {
  programs.kitty = {
    enable = true;
    font.name = "Comic Code";
    settings = {
      font_size = 12;
      bold_font = "auto";
      italic_font = "auto";
      bold_italic_font = "auto";
      background_opacity = "0.9";
      disable_ligatures = "cursor";
      enable_audio_bell = false;
      tab_bar_style = "separator";
      tab_separator = " | ";
      confirm_os_window_close = 0;
    };
    extraConfig = "include ${./kitty.theme}";
  };
}
