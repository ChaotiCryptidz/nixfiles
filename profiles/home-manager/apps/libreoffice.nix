{pkgs, ...}: {
  home.packages = with pkgs; [
    libreoffice
    hunspell
    hunspellDicts.tok # Toki Pona
    hunspellDicts.en_GB-ize # GB English with -ize endings
    hunspellDicts.en-us # US English
    hunspellDicts.de-de # German
    hunspellDicts.de-at # Austrian German
  ];
}
