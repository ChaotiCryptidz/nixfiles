{pkgs, ...}: {
  home.packages = [
    (pkgs.wrapOBS.override {} {
      plugins = with pkgs.obs-studio-plugins; [
        obs-vkcapture
        input-overlay
        obs-pipewire-audio-capture
      ];
    })
  ];
}
