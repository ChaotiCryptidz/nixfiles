{
  self,
  pkgs,
  ...
}: let
  encryptedUSBData = import "${self}/data/drives/encryptedUSB.nix";
in {
  home.packages = with pkgs; [age];

  programs.zsh.shellAliases = {
    chaos_age = "age -i ${encryptedUSBData.chaosAgePrivateKeyPath}";
    chaos_age_encrypt = "age -a -e -i ${encryptedUSBData.chaosAgePrivateKeyPath}";
    chaos_pub = "cat ${encryptedUSBData.chaosAgePublicKeyPath}";
  };
}
