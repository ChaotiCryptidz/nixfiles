{pkgs, ...}: {
  home.packages = with pkgs; [
    (prismlauncher.override {
      jdks = with pkgs; [jdk8 jdk11 jdk17 jdk21];
      withWaylandGLFW = false; # TODO: won't build with
    })
  ];
}
