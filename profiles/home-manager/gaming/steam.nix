{pkgs, ...}: {
  home.packages = with pkgs; [
    steam
    steamtinkerlaunch
  ];
}
