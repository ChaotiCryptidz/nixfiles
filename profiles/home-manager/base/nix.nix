{pkgs, ...}: {
  home.packages = with pkgs; [
    nix-tree
    nix-output-monitor
  ];
}
