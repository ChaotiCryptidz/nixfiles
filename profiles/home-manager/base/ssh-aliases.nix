{lib, ...}: let
  inherit (lib.modules) mkMerge;
in {
  programs.ssh = {
    enable = true;
    matchBlocks = mkMerge [
      (mkMerge (map (hostname: {
        "${hostname}" = {
          user = "root";
          hostname = "${hostname}.servers.genderfucked.monster";
        };
      }) ["hetzner-arm" "hetzner-arm-decrypt" "raspberry-pi5" "raspberry-pi5-decrypt"]))
      {
        "blahaj" = {
          user = "chaos";
          hostname = "blahaj.sapphicco.de";
        };
      }
    ];
  };
}
