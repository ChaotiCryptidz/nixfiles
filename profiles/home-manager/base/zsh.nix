{pkgs, ...}: {
  home.packages = with pkgs; [bat ripgrep];
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    autosuggestion.enable = true;
    oh-my-zsh = {
      enable = true;
      theme = "afowler";
      plugins = [];
    };
    shellAliases = {
      ip6 = "ip -6";

      sys = "systemctl";
      sysu = "systemctl --user";
      log = "journalctl";
      logu = "journalctl --user";

      dmesg = "dmesg -HP";

      hg = "history 0 | rg";
    };
  };
  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
  };
}
