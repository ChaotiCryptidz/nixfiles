{pkgs, ...}: {
  home.packages = with pkgs; [vault];

  programs.zsh.envExtra = ''
    export VAULT_ADDR="https://vault.owo.monster"
  '';

  home.sessionVariables = {
    VAULT_ADDR = "https://vault.owo.monster";
  };
}
