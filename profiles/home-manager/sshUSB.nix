{self, ...}: let
  encryptedUSBData = import "${self}/data/drives/encryptedUSB.nix";
in {
  programs.ssh.matchBlocks."*".identityFile = "${encryptedUSBData.sshPrivateKeyPath}";
  programs.git.extraConfig = {
    gpg.format = "ssh";
    commit.gpgsign = "true";
    tag.gpgsign = "true";
    user.signingKey = "${encryptedUSBData.sshPrivateKeyPath}";
  };
}
