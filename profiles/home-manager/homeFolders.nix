{config, ...}: {
  systemd.user.tmpfiles.rules = map (dir: "d ${config.home.homeDirectory}/${dir} - ${config.home.username} users") [
    "Projects"
    "Temp"
    "Music"
    "Mounts"
    "Mounts/Storage"
    "Mounts/Storage-Public"
    "Mounts/Backups"
    "Mounts/Backups-Crypt"
    "Mounts/Photos-Crypt"
    "Mounts/Personal-Crypt"
    "Mounts/Notes-Crypt"
    "Mounts/Public"
    "Mounts/Temp1"
    "Mounts/Temp2"
  ];
}
