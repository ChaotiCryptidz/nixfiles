{...}: {
  imports = [
    ./encryptedDriveMod/filesystems.nix
    ./encryptedDriveMod/kernelModules.nix
    ./encryptedDriveMod/options.nix
    ./encryptedDriveMod/passwordMode.nix
    ./encryptedDriveMod/sshMode.nix
    ./encryptedDriveMod/usbMode.nix
  ];
}
