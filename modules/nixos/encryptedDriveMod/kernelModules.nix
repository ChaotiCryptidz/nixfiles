{
  config,
  lib,
  pkgs,
  ...
}: let
  inherit (lib.modules) mkIf;
  inherit (lib.lists) flatten optionals;

  cfg = config.boot.encryptedDrive;
in {
  config = mkIf cfg.enable {
    boot.initrd.availableKernelModules =
      flatten
      [
        # For USB w/ Encryption Key
        "usb_storage"
        "usbcore"
        "uas"
        "rtsx_pci_sdmmc"
        # For USB Keyboards
        "usbhid"
        "hid_generic"
        # For Cryptography
        "cryptd"

        (optionals (pkgs.system == "x86_64_linux") [
          "aesni_intel"
          "crypto_simd"
        ])
      ];
  };
}
