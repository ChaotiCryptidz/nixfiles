{
  config,
  lib,
  self,
  ...
}: let
  inherit (lib.modules) mkIf;

  driveData = import "${self}/data/drives/encryptedDrive.nix";

  cfg = config.boot.encryptedDrive;
in {
  config = mkIf cfg.enable {
    boot.initrd.luks.devices = {
      "${driveData.mapperName}" = {
        device = "${driveData.encryptedPath}";
        preLVM = false;
        allowDiscards = true;
        fallbackToPassword = cfg.allowPasswordDecrypt;
      };
    };

    fileSystems = {
      "/" = {
        device = "${driveData.decryptedPath}";
        fsType = "${driveData.unencryptedFSType}";
      };
      "/boot" = {
        device = "${driveData.bootPath}";
        fsType = "${driveData.bootFSType}";
      };
    };
  };
}
