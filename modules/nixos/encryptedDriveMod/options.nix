{lib, ...}: let
  inherit (lib.options) mkOption mkEnableOption;
  inherit (lib) types;
in {
  options.boot.encryptedDrive = {
    enable = mkEnableOption "encrypted drive support for servers and other devices";

    mode = mkOption {
      type = types.enum [
        "encrypted-usb"
        "password"
        "ssh"
      ];
      default = "encrypted-usb";
    };

    allowPasswordDecrypt = mkOption {
      description = "allow fallback to decrypting with a password when using USB based auth, pass cryptsetup_password to kernel cmdline to switch to password input mode";
      type = types.bool;
      default = true;
    };
  };
}
