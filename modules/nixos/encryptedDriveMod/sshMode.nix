{
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkIf mkForce;

  cfg = config.boot.encryptedDrive;
in {
  config = mkIf (cfg.enable && cfg.mode == "ssh") {
    boot.loader.supportsInitrdSecrets = true;

    boot.initrd.secrets = {
      # we only support ed25519 for now
      "/ssh_host_ed25519_key" = mkForce "/initrd_ssh_host_ed25519_key";
    };

    boot.initrd.luks.forceLuksSupportInInitrd = true;

    boot.initrd.network = {
      enable = true;
      ssh = {
        enable = true;
        port = 22;
        authorizedKeys = config.users.users.root.openssh.authorizedKeys.keys;
        hostKeys = ["/ssh_host_ed25519_key"];
      };
      postCommands = ''
        echo 'cryptsetup-askpass' >> /root/.profile
      '';
    };
  };
}
