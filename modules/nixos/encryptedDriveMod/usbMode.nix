{
  config,
  lib,
  self,
  ...
}: let
  inherit (lib.modules) mkIf mkBefore;

  encryptedUSB = import "${self}/data/drives/encryptedUSB.nix";
  driveData = import "${self}/data/drives/encryptedDrive.nix";

  cfg = config.boot.encryptedDrive;
in {
  config = mkIf (cfg.enable && cfg.mode == "encrypted-usb") {
    boot = {
      initrd.postDeviceCommands = mkBefore ''
        mkdir -m 0755 -p /keys
        mkdir -m 0755 -p ${encryptedUSB.mountpoint}

        ${
          if cfg.allowPasswordDecrypt
          then ''
            if grep "cryptsetup_password" /proc/cmdline; then
              USE_PASSWORD_FALLBACK=true
            else
              USE_PASSWORD_FALLBACK=false
            fi
          ''
          else ''
            USE_PASSWORD_FALLBACK=false
          ''
        }

        while !(test -b ${encryptedUSB.encryptedPath}) && [ "$USE_PASSWORD_FALLBACK" == "false" ]
        do
          echo "Please Plug In USB"
          sleep 1
        done

        if [ "$USE_PASSWORD_FALLBACK" == "true" ]; then
          echo "Please Decrypt Drive"
          cryptsetup open ${driveData.encryptedPath} ${driveData.mapperName}
        else
          echo "Please Decrypt USB"
          cryptsetup open ${encryptedUSB.encryptedPath} ${encryptedUSB.preBootMapperName}
        fi

        if [ "$USE_PASSWORD_FALLBACK" == "false" ]; then
          mount -n -t ${encryptedUSB.unencryptedFSType} -o ro ${encryptedUSB.preBootMapperPath} ${encryptedUSB.mountpoint}

          cp ${encryptedUSB.encryptionKeysPath}/${config.networking.hostName}.key /keys
          chmod 0755 /keys/${config.networking.hostName}.key
          umount -f ${encryptedUSB.mountpoint}

          cryptsetup close ${encryptedUSB.preBootMapperName}
        else
          touch /keys/${config.networking.hostName}.key
        fi
      '';
      initrd.luks.devices = {
        "${driveData.mapperName}" = {
          keyFile = "/keys/${config.networking.hostName}.key";
        };
      };
    };
  };
}
