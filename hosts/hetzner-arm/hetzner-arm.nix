{
  tree,
  lib,
  ...
}: let
  inherit (lib.lists) forEach flatten;
in {
  imports = flatten (with tree; [
    (with tree.presets.nixos; [
      serverBase
      serverHetzner
      serverEncryptedDrive
      kernelLatest
    ])

    profiles.nixos.nginx

    (forEach [
      "storage"
      "mail"
      #"jellyfin"
    ] (name: ./containers + "/${name}/${name}.nix"))

    (with hosts.hetzner-arm.profiles; [
      staticSites
      gotosocial
      forgejo
      mpd
      radicale
      vault
      restic
    ])

    ./hardware.nix
    ./secrets.nix
  ]);

  nixpkgs.overlays = [
    (_final: prev: {
      # So we don't need to build all Vault
      # when we already are using vault-bin on this server
      vault = prev.vault-bin;

      # Have no need for HW Accel, hoping it works with this
      jellyfin-ffmpeg = prev.ffmpeg_6-headless;

      ffmpeg = prev.ffmpeg-headless;
      ffmpeg_4 = prev.ffmpeg_4-headless;
      ffmpeg_5 = prev.ffmpeg_5-headless;
      ffmpeg_6 = prev.ffmpeg_6-headless;
      ffmpeg_7 = prev.ffmpeg_7-headless;

      mpd = prev.mpd-headless;
    })
  ];

  # For Containers
  networking.nat = {
    enable = true;
    internalInterfaces = ["ve-+"];
    externalInterface = "enp1s0";
  };

  networking.firewall = {
    allowedTCPPorts = [80 443];
    allowedUDPPorts = [80 443];
  };

  networking.hostName = "hetzner-arm";

  home-manager.users.root.home.stateVersion = "24.05";
  system.stateVersion = "24.05";
}
