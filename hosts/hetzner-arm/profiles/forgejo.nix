{pkgs, ...}: {
  services.forgejo = {
    enable = true;
    database = {
      type = "sqlite3";
    };
    lfs.enable = true;
    settings = {
      DEFAULT.APP_NAME = "chaos's Forgejo";
      server = rec {
        DOMAIN = "forgejo.owo.monster";
        ROOT_URL = "https://${DOMAIN}";
        # Can't access /run out of container
        HTTP_ADDR = "/var/sockets/forgejo/forgejo.sock";
        PROTOCOL = "http+unix";
        START_SSH_SERVER = true;
        SSH_PORT = 2222;
        SSH_LISTEN_PORT = SSH_PORT;
        OFFLINE_MODE = true;
        ENABLE_GZIP = true;
      };
      repository = {
        DISABLED_REPO_UNITS = "repo.ext_issues,repo.pulls,repo.wiki,repo.ext_wiki,repo.projects,repo.packages";
        DEFAULT_REPO_UNITS = "repo.code,repo.releases,repo.issues,repo.actions";
      };
      ui = {
        DEFAULT_THEME = "forgejo-dark";
      };
      "ui.meta" = {
        AUTHOR = "chaos's Forgejo";
        DESCRIPTION = "chaos's personal Forgejo instance";
        KEYWORDS = "";
      };
      indexer = {
        REPO_INDEXER_ENABLED = true;
      };
      service = {
        DISABLE_REGISTRATION = true;
      };
      "ssh.minimum_key_sizes" = {
        ECDSA = -1;
        # RSA currently enabled for spicywolf
        # RSA = -1;
        DSA = -1;
      };
      session = {
        PROVIDER = "db";
      };
      oauth = {
        ENABLE = false;
      };
      time = {
        DEFAULT_UI_LOCATION = "Europe/London";
      };
      packages = {
        ENABLE = false;
      };
    };
  };

  environment.systemPackages = [
    (pkgs.writeShellScriptBin "forgejo" ''
      sudo -u forgejo ${pkgs.forgejo}/bin/gitea -w /var/lib/forgejo "$@"
    '')
  ];

  systemd.services.forgejo.serviceConfig.ReadWritePaths = [
    "/var/sockets/forgejo"
  ];

  systemd.tmpfiles.rules = [
    "d /var/sockets - root root"
    "d /var/sockets/forgejo - forgejo forgejo"
  ];

  services.nginx = {
    enable = true;
    virtualHosts."forgejo.owo.monster" = {
      enableACME = true;
      forceSSL = true;
      locations."/".proxyPass = "http://unix:/var/sockets/forgejo/forgejo.sock";
    };
  };

  networking.firewall.allowedTCPPorts = [
    2222
  ];
}
