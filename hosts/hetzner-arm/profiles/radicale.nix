{config, ...}: let
  inherit (config.services.secrets) secrets;
in {
  services.radicale = {
    enable = true;
    settings = {
      server = {
        hosts = ["127.0.0.1:5232"];
      };
      auth = {
        type = "htpasswd";
        htpasswd_filename = "${secrets.radicale_htpasswd.path}";
        htpasswd_encryption = "bcrypt";
      };
      rights = {
        type = "owner_only";
      };
      storage = {
        filesystem_folder = "/var/lib/radicale/collections";
      };
    };
  };

  systemd.tmpfiles.rules = [
    "d /var/lib/radicale - radicale radicale"
    "d /var/lib/radicale/collections - radicale radicale"
  ];

  users.users.radicale.uid = 1002;
  users.groups.radicale.gid = 1002;

  services.nginx = {
    enable = true;
    virtualHosts."radicale.owo.monster" = {
      enableACME = true;
      forceSSL = true;
      locations."/".proxyPass = "http://127.0.0.1:5232";
    };
  };
}
