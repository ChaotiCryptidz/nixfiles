{
  inputs,
  pkgs,
  ...
}: {
  services.vaultui = {
    enable = true;
    package = inputs.vaultui.packages.${pkgs.system}.vaultui;
    domain = "vaultui.owo.monster";
  };

  services.nginx.virtualHosts."thisisanexampleofspeex.uk" = {
    enableACME = true;
    forceSSL = true;
    root = ./staticSiteData/speex;
  };

  services.nginx.virtualHosts."contact.owo.monster" = {
    enableACME = true;
    forceSSL = true;
    root = ./staticSiteData/contact;
  };

  services.nginx.virtualHosts."food.owo.monster" = {
    enableACME = true;
    forceSSL = true;
    root = "${inputs.food-site.packages.${pkgs.system}.food-site}/share/food-site";
  };

  services.nginx.virtualHosts."musiclibrary.owo.monster" = {
    enableACME = true;
    forceSSL = true;
    root = "/var/lib/static-sites/music_library";
  };

  systemd.services.nginx.serviceConfig.ReadWritePaths = [
    "/var/lib/static-sites"
  ];

  systemd.tmpfiles.rules = [
    "d /var/lib/static-sites - nginx nginx"
    "d /var/lib/static-sites/music_library - nginx nginx"
  ];
}
