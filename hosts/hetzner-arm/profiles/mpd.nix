{
  lib,
  pkgs,
  config,
  ...
}: let
  inherit (lib.strings) concatStringsSep;
  inherit (lib.lists) forEach;
  inherit (lib.modules) mkMerge;

  ports = {
    mpd = 6600;
    mpd-opus-low = 4242;
    mpd-opus-medium = 4243;
    mpd-opus-high = 4244;
    mpd-flac = 4245;
  };

  inherit (config.services.secrets) secrets;
in {
  environment.systemPackages = with pkgs; [
    mpc_cli
  ];

  systemd.tmpfiles.rules = [
    "d /Music - mpd mpd"
  ];

  systemd.services.mpd = {
    serviceConfig = {
      ReadOnlyPaths = "/Music";
    };
  };

  services.mpd = {
    enable = true;
    network.listenAddress = "any";
    musicDirectory = "/Music";
    dbFile = null;
    credentials = [
      {
        passwordFile = "${secrets.mpd_control_password.path}";
        permissions = ["read" "add" "control" "admin"];
      }
    ];
    extraConfig =
      ''
        bind_to_address "0.0.0.0"
        #bind_to_address "::"
        host_permissions "127.0.0.1 read,add,control,admin"
        metadata_to_use "title,artist"
        auto_update "yes"
        audio_buffer_size "4096"
        replaygain "track"
        audio_output_format "48000:24:2"
        resampler {
          plugin "soxr"
          quality "very high"
          threads "0"
        }
        database {
            plugin "simple"
            path "/var/lib/mpd/db"
        }
      ''
      + concatStringsSep "\n" (forEach ["low" "medium" "high"] (quality: let
        bitrates = {
          "low" = "64";
          "medium" = "96";
          "high" = "128";
        };
        bitrate = bitrates.${quality};
      in ''
        audio_output {
          type "httpd"
          name "http (opus-${bitrate}k) /opus/${quality}"
          encoder "opus"
          port "${toString ports."mpd-opus-${quality}"}"
          bitrate "${bitrate}000"
          format "48000:24:2"
          always_on "yes"
          tags "yes"
          signal "music"
        }
      ''))
      + ''
        audio_output {
          type "httpd"
          name "http (flac) /flac"
          encoder "flac"
          port "${toString ports.mpd-flac}"
          format "48000:24:2"
          always_on "yes"
          tags "yes"
        }
      '';
  };

  services.nginx.virtualHosts."mpd.owo.monster" = let
    extraConfig = ''
      auth_basic "Music Password";
      auth_basic_user_file ${secrets.music_stream_passwd.path};
    '';
  in {
    forceSSL = true;
    enableACME = true;
    locations = mkMerge [
      {
        "/flac" = {
          proxyPass = "http://127.0.0.1:${toString ports.mpd-flac}";
          inherit extraConfig;
        };
      }
      (mkMerge (forEach ["low" "medium" "high"] (quality: {
        "/opus-${quality}" = {
          proxyPass = "http://127.0.0.1:${toString ports."mpd-opus-${quality}"}";
          inherit extraConfig;
        };
      })))
    ];
  };

  networking.firewall.allowedTCPPorts = with ports; [
    mpd
  ];
}
