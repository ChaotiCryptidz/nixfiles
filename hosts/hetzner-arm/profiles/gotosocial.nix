{config, ...}: let
  inherit (config.services.secrets) secrets;
in {
  services.gotosocial = {
    enable = true;
    setupPostgresqlDB = false;
    environmentFile = secrets.gotosocial_env.path;

    settings = {
      application-name = "chaos-gts";
      host = "gts-02.owo.monster";
      bind-address = "0.0.0.0";

      log-level = "info";
      log-client-ip = true;

      db-type = "sqlite";
      db-address = "/var/lib/gotosocial/sqlite.db";

      trusted-proxies = [
        "127.0.0.1/32"
        "::1"
      ];

      port = 8080;
      letsencrypt-enabled = false;

      accounts-registration-open = false;

      smtp-host = "mail.owo.monster";
      smtp-port = 587;
      smtp-from = "gotosocial@owo.monster";
      smtp-username = "gotosocial@owo.monster";
      smtp-password = ""; # set via env variables

      instance-languages = ["en" "de"];

      media-image-max-size = "64MB";
      media-video-max-size = "1024MB";
      media-description-max-chars = 4000;
      media-emoji-local-max-size = "4MB";
      media-emoji-remote-max-size = "2MB";

      media-remote-cache-days = 1;
      media-cleanup-every = "6h";

      statuses-poll-max-options = 10;
      statuses-media-max-files = 8;

      advanced-rate-limit-requests = 0;

      # only enable when testing out mastodon-api applications
      # that use instance version for api compatibility checks
      # instance-inject-mastodon-version = true;

      cache = {
        memory-target = "128MiB";
      };
    };
  };

  services.nginx.virtualHosts."gts-02.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:8080";
      proxyWebsockets = true;
      extraConfig = ''
        # uncomment if running nginx without recommendedProxySettings
        # proxy_set_header Host $host;
        # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # proxy_set_header X-Forwarded-Proto $scheme;
      '';
    };
    extraConfig = ''
      client_max_body_size 128M;
    '';
  };
}
