{pkgs, ...}: {
  services.secrets = {
    enable = true;

    vaultLogin = {
      enable = true;
      loginUsername = "hetzner-arm";
    };

    packages = with pkgs; [
      apacheHttpd
    ];

    requiredVaultPaths = [
      "private-public-keys/data/ssh/root@hetzner-arm"
      "private-public-keys/data/ssh/root@hetzner-arm-decrypt"

      "api-keys/data/backblaze/Chaos-Backups"

      "private-public-keys/data/restic/Social"
      "api-keys/data/chaos_mail/gotosocial"

      "private-public-keys/data/restic/Forgejo"

      "api-keys/data/mpd"
      "api-keys/data/music-stream"

      "api-keys/data/radicale"
      "private-public-keys/data/restic/Radicale"

      "private-public-keys/data/restic/Vault"
    ];

    secrets = {
      vault_password = {
        manual = true;
      };

      ssh_host_ed25519_key = {
        path = "/etc/ssh/ssh_host_ed25519_key";
        permissions = "600";
        fetchScript = ''
          [ ! -d "$SYSROOT/etc/ssh" ] && mkdir -p "$SYSROOT/etc/ssh/"
          simple_get "/private-public-keys/ssh/root@hetzner-arm" .private | base64 -d > "$secretFile"
        '';
      };
      ssh_host_ed25519_key_pub = {
        path = "/etc/ssh/ssh_host_ed25519_key.pub";
        permissions = "600";
        fetchScript = ''
          [ ! -d "$SYSROOT/etc/ssh" ] && mkdir -p "$SYSROOT/etc/ssh/"
          simple_get "/private-public-keys/ssh/root@hetzner-arm" .private | base64 -d > "$secretFile"
        '';
      };

      # this doesn't need to be a secret and can be generated at install time
      # but it makes it easier to install.
      # it's stored in /nix store anyway
      initrd_ssh_host_ed25519_key = {
        path = "/initrd_ssh_host_ed25519_key";
        permissions = "600";
        fetchScript = ''
          simple_get "/private-public-keys/ssh/root@hetzner-arm-decrypt" .private | base64 -d > "$secretFile"
        '';
      };

      # B2 Keys for all backups
      restic_backups_env = {
        fetchScript = ''
          cat << EOF > "$secretFile"
          AWS_ACCESS_KEY_ID=$(simple_get "/api-keys/backblaze/Chaos-Backups" .keyID)
          AWS_SECRET_ACCESS_KEY=$(simple_get "/api-keys/backblaze/Chaos-Backups" .applicationKey)
          EOF
        '';
      };

      restic_password_social = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Social" .password > "$secretFile"
        '';
      };

      gotosocial_env = {
        fetchScript = ''
          smtp_password=$(simple_get "/api-keys/chaos_mail/gotosocial" .password)
          echo "GTS_SMTP_PASSWORD=$smtp_password" > "$secretFile"
        '';
      };

      restic_password_forgejo = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Forgejo" .password > "$secretFile"
        '';
      };

      mpd_control_password = {
        user = "mpd";
        group = "mpd";
        fetchScript = ''
          simple_get "/api-keys/mpd" .password > "$secretFile"
        '';
      };

      music_stream_passwd = {
        user = "nginx";
        group = "nginx";
        fetchScript = ''
          username=$(simple_get "/api-keys/music-stream" .username)
          password=$(simple_get "/api-keys/music-stream" .password)
          htpasswd -bc "$secretFile" "$username" "$password" 2>/dev/null
        '';
      };

      radicale_htpasswd = {
        user = "radicale";
        group = "radicale";
        fetchScript = ''
          if [ -f "$secretFile" ]; then
            rm "$secretFile"
          fi

          touch "$secretFile"

          data=$(kv_get "/api-keys/radicale" | base64)
          for username in $(echo "$data" | base64 -d | jq -r ".data.data | keys | .[]"); do
            password=$(echo "$data" | base64 -d | jq -r ".data.data.\"$username\"")
            htpasswd -bB "$secretFile" "$username" "$password" 2>/dev/null
          done
        '';
      };

      restic_password_radicale = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Radicale" .password > "$secretFile"
        '';
      };

      restic_password_vault = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Vault" .password > "$secretFile"
        '';
      };
    };
  };
}
