{
  self,
  config,
  ...
}: let
  inherit (config.services.secrets) secrets;
  backupSchedules = import "${self}/data/backupSchedules.nix";
in {
  services.restic.backups.jellyfin = {
    user = "root";
    paths = [
      "/var/lib/jellyfin"
    ];

    repository = "s3:s3.eu-central-003.backblazeb2.com/Chaos-Backups/Restic/Jellyfin";
    passwordFile = "${secrets.restic_password.path}";
    environmentFile = "${secrets.restic_env.path}";
    createWrapper = true;

    pruneOpts = ["--keep-last 10"];
    timerConfig = backupSchedules.restic.low;
  };
}
