{...}: {
  services.jellyfin = {
    enable = true;
    openFirewall = true;
  };
  users.users.jellyfin.uid = 1000;
  users.groups.jellyfin.gid = 1000;
}
