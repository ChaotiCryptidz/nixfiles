{
  self,
  tree,
  inputs,
  pkgs,
  config,
  lib,
  ...
}: let
  inherit (lib.lists) flatten;

  containerName = "jellyfin";

  containerAddresses = import "${self}/hosts/hetzner-arm/data/containerAddresses.nix";

  hostIP = containerAddresses.host;
  containerIP = containerAddresses.containers.${containerName};
in {
  containers.jellyfin = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = hostIP;
    localAddress = containerIP;

    bindMounts = {
      "/dev/fuse" = {
        hostPath = "/dev/fuse";
        isReadOnly = false;
      };
    };

    # Allow rclone mount in container
    allowedDevices = [
      {
        modifier = "rwm";
        node = "/dev/fuse";
      }
      {
        modifier = "rwm";
        node = "/dev/mapper/control";
      }
    ];

    specialArgs = {
      inherit inputs;
      inherit tree;
      inherit self;
    };

    config = {...}: {
      nixpkgs.pkgs = pkgs;

      imports = flatten (with tree; [
        presets.nixos.containerBase

        (with hosts.hetzner-arm.containers.jellyfin.profiles; [
          mediaMount
          jellyfin
          restic
        ])

        ./secrets.nix
      ]);

      home-manager.users.root.home.stateVersion = "24.05";
      system.stateVersion = "24.05";
    };
  };

  services.nginx.virtualHosts."jellyfin.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    extraConfig = ''
      client_max_body_size 512M;

      # Security / XSS Mitigation Headers
      # NOTE: X-Frame-Options may cause issues with the webOS app
      add_header X-Frame-Options "SAMEORIGIN";
      add_header X-XSS-Protection "0"; # Do NOT enable. This is obsolete/dangerous
      add_header X-Content-Type-Options "nosniff";

      # COOP/COEP. Disable if you use external plugins/images/assets
      add_header Cross-Origin-Opener-Policy "same-origin" always;
      add_header Cross-Origin-Embedder-Policy "require-corp" always;
      add_header Cross-Origin-Resource-Policy "same-origin" always;

      # Permissions policy. May cause issues on some clients
      add_header Permissions-Policy "accelerometer=(), ambient-light-sensor=(), battery=(), bluetooth=(), camera=(), clipboard-read=(), display-capture=(), document-domain=(), encrypted-media=(), gamepad=(), geolocation=(), gyroscope=(), hid=(), idle-detection=(), interest-cohort=(), keyboard-map=(), local-fonts=(), magnetometer=(), microphone=(), payment=(), publickey-credentials-get=(), serial=(), sync-xhr=(), usb=(), xr-spatial-tracking=()" always;

      # Tell browsers to use per-origin process isolation
      add_header Origin-Agent-Cluster "?1" always;
    '';

    locations."/" = {
      proxyPass = "http://${containerIP}:8096";
      proxyWebsockets = true;
      extraConfig = ''
        proxy_buffering off;
      '';
    };
  };
}
