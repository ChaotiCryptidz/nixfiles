{pkgs, ...}: {
  services.secrets = {
    enable = true;

    packages = with pkgs; [
      # for rclone obscure config file
      rclone
    ];

    extraFunctions = ''
      replace_slash_for_sed() {
        sed "s#/#\\\/#"
      }

      simple_get_obscure() {
        rclone obscure "$(simple_get "$@")"
      }

      simple_get_replace_b2() {
        api_account=$(simple_get "$1" .keyID | replace_slash_for_sed)
        api_key=$(simple_get "$1" .applicationKey | replace_slash_for_sed)

        replace_account=''${2}_ACCOUNT
        replace_key=''${2}_KEY

        sed -i "s/$replace_account/$api_account/" "$3"
        sed -i "s/$replace_key/$api_key/" "$3"
      }

      simple_get_replace_crypt() {
        password=$(simple_get_obscure "$1" .password)
        salt=$(simple_get_obscure "$1" .salt)

        replace_password=''${2}_PASSWORD
        replace_salt=''${2}_SALT

        sed -i "s/$replace_password/$password/" "$3"
        sed -i "s/$replace_salt/$salt/" "$3"
      }
    '';

    vaultLogin = {
      enable = true;
      loginUsername = "hetzner-arm-container-jellyfin";
    };

    requiredVaultPaths = [
      "api-keys/data/backblaze/Chaos-Media"
      "api-keys/data/putio"
      "private-public-keys/data/rclone/Chaos-Media-Crypt"

      "api-keys/data/backblaze/Chaos-Backups"
      "private-public-keys/data/restic/Jellyfin"
    ];

    secrets = {
      vault_password = {
        manual = true;
      };

      rclone_config = {
        user = "jellyfin";
        group = "jellyfin";
        fetchScript = ''
          cp ${./data/rclone_config.template} "$secretFile"

          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Media" "B2_CHAOS_MEDIA" "$secretFile"

          PUTIO_PASSWORD="token/$(simple_get /api-keys/putio .oauth_token)"
          PUTIO_PASSWORD="$(rclone obscure "$PUTIO_PASSWORD")"
          sed -i "s/PUTIO_PASSWORD/$PUTIO_PASSWORD/" "$secretFile"

          simple_get_replace_crypt "/private-public-keys/rclone/Chaos-Media-Crypt" "STORAGE_MEDIA_CRYPT" "$secretFile"
        '';
      };

      restic_password = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Jellyfin" .password > "$secretFile"
        '';
      };
      restic_env = {
        fetchScript = ''
          cat << EOF > "$secretFile"
          AWS_ACCESS_KEY_ID=$(simple_get "/api-keys/backblaze/Chaos-Backups" .keyID)
          AWS_SECRET_ACCESS_KEY=$(simple_get "/api-keys/backblaze/Chaos-Backups" .applicationKey)
          EOF
        '';
      };
    };
  };
}
