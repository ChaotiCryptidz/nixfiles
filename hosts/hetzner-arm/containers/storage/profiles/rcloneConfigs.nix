{config, ...}: let
  inherit (config.services.secrets) secrets;
in {
  systemd.tmpfiles.rules = [
    "d /root/.config - root root"
    "d /root/.config/rclone - root root"
    "L /root/.config/rclone/rclone.conf - - - - ${secrets.rclone_config.path}"

    "d /home/storage/.config - storage storage"
    "d /home/storage/.config/rclone - storage storage"
    "L /home/storage/.config/rclone/rclone.conf - - - - ${secrets.rclone_config.path}"
  ];
}
