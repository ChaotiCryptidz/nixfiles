{config, ...}: let
  inherit (config.services.secrets) secrets;
  ports = import ../data/ports.nix;
in {
  systemd.tmpfiles.rules = [
    "d /caches - storage storage"
    "d /caches/main_webdav_serve - storage storage"
    "d /caches/media_webdav_serve - storage storage"
  ];

  services.rclone-serve = {
    enable = true;
    remotes = map (remote:
      {
        user = "storage";
      }
      // remote) [
      {
        id = "main";
        remote = "Storage:";
        type = "webdav";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.webdav_main}"
          "--htpasswd=${secrets.webdav_main_htpasswd.path}"
          "--baseurl=/Main/"
          "--cache-dir=/caches/main_webdav_serve"
          "--vfs-cache-mode=full"
        ];
      }
      {
        id = "music-ro";
        remote = "Storage:Music";
        type = "webdav";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.webdav_music_ro}"
          "--read-only"
          "--baseurl=/MusicRO/"
        ];
      }
      {
        id = "uploads";
        remote = "Storage:Uploads";
        type = "webdav";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.webdav_uploads}"
          "--htpasswd=${secrets.webdav_uploads_htpasswd.path}"
          "--baseurl=/Uploads/"
        ];
      }
      {
        id = "notes";
        remote = "Notes:";
        type = "webdav";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.webdav_notes}"
          "--htpasswd=${secrets.webdav_notes_htpasswd.path}"
          "--baseurl=/Notes/"
        ];
      }
      {
        id = "music-ro";
        remote = "Storage:Music";
        type = "http";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.http_music}"
          "--baseurl=/Music/"
          "--read-only"
        ];
      }
      {
        id = "public";
        remote = "Storage:Public";
        type = "http";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.http_public}"
          "--baseurl=/Public/"
          "--read-only"
        ];
      }
      {
        id = "uploads-public";
        remote = "Storage:Uploads/Public";
        type = "http";
        extraArgs = [
          "--addr=0.0.0.0:${toString ports.http_uploads_public}"
          "--baseurl=/Uploads/"
          "--read-only"
        ];
      }
    ];
  };
}
