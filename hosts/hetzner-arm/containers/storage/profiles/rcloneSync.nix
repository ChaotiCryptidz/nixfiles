{self, ...}: let
  backupSchedules = import "${self}/data/backupSchedules.nix";
in {
  services.rclone-sync = {
    enable = true;
    user = "storage";
    syncJobs = map (syncJob:
      syncJob
      // {
        timerConfig = backupSchedules.remoteBackups;
        extraArgs = [
          "--fast-list"
          "--check-first"
          "--delete-before"
          "--b2-upload-concurrency=4"
          "--transfers=4"
          "--bwlimit 80M"
        ];
      }) [
      # Pheonix System's B2
      {
        source = "Storage:Backups";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Backups";
        id = "phoenix_b2_backups";
      }
      {
        source = "Storage:Photos";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Photos";
        id = "phoenix_b2_photos";
      }
      {
        source = "Storage:Music";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Music";
        id = "phoenix_b2_music";
      }
      {
        source = "Storage:Personal";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Personal";
        id = "phoenix_b2_personal";
      }
      {
        source = "Storage:Public";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Public";
        id = "phoenix_b2_public";
      }
      {
        source = "B2-Chaos-Notes:Chaos-Notes";
        dest = "B2-Phoenix-Cryptidz-Storage:Phoenix-Cryptidz-Storage/Notes";
        id = "phoenix_b2_notes";
      }
    ];
  };
}
