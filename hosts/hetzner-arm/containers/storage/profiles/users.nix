{...}: {
  users.groups.storage = {
    gid = 1000;
  };
  users.users.storage = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = ["storage"];
  };
}
