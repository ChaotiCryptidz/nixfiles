{
  self,
  tree,
  lib,
  inputs,
  pkgs,
  ...
}: let
  inherit (lib.attrsets) attrValues;
  inherit (lib.lists) flatten;

  containerAddresses = import "${self}/hosts/hetzner-arm/data/containerAddresses.nix";
  hostIP = containerAddresses.host;
  containerIP = containerAddresses.containers.storage;

  # 32GB
  clientMaxBodySize = "${toString (8192 * 4)}M";

  ports = import ./data/ports.nix;
in {
  containers.storage = {
    autoStart = true;
    privateNetwork = true;
    hostAddress = hostIP;
    localAddress = containerIP;

    bindMounts = {
      "/dev/fuse" = {
        hostPath = "/dev/fuse";
        isReadOnly = false;
      };
    };

    # Allow rclone mount in container
    allowedDevices = [
      {
        modifier = "rwm";
        node = "/dev/fuse";
      }
      {
        modifier = "rwm";
        node = "/dev/mapper/control";
      }
    ];

    specialArgs = {
      inherit inputs;
      inherit tree;
      inherit self;
    };

    config = {...}: {
      nixpkgs.pkgs = pkgs;

      imports = flatten (with tree; [
        presets.nixos.containerBase

        (with hosts.hetzner-arm.containers.storage.profiles; [
          rcloneConfigs
          rcloneServe
          rcloneSync
          users
        ])

        ./secrets.nix
      ]);

      environment.systemPackages = with pkgs; [
        rclone

        fuse
        fuse3
      ];

      networking.firewall = {
        enable = true;
        allowedTCPPorts = attrValues ports;
      };

      home-manager.users.root.home.stateVersion = "24.05";
      system.stateVersion = "24.05";
    };
  };

  services.nginx.virtualHosts."storage-webdav.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/Main/".proxyPass = "http://${containerIP}:${toString ports.webdav_main}";
      "/MusicRO/".proxyPass = "http://${containerIP}:${toString ports.webdav_music_ro}";
      "/Uploads/".proxyPass = "http://${containerIP}:${toString ports.webdav_uploads}";
      "/Notes/".proxyPass = "http://${containerIP}:${toString ports.webdav_notes}";
    };
    extraConfig = ''
      client_max_body_size ${clientMaxBodySize};
    '';
  };

  services.nginx.virtualHosts."storage-http.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/Music/".proxyPass = "http://${containerIP}:${toString ports.http_music}";
      "/Public/".proxyPass = "http://${containerIP}:${toString ports.http_public}";
      "/Uploads/".proxyPass = "http://${containerIP}:${toString ports.http_uploads_public}";
    };
    extraConfig = ''
      client_max_body_size ${clientMaxBodySize};
    '';
  };
}
