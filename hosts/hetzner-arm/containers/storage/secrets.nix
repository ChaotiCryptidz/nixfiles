{pkgs, ...}: {
  services.secrets = {
    enable = true;

    vaultLogin = {
      enable = true;
      loginUsername = "hetzner-arm-container-storage";
    };

    requiredVaultPaths = [
      "api-keys/data/backblaze/Chaos-Backups"
      "api-keys/data/backblaze/Chaos-Photos"
      "api-keys/data/backblaze/Chaos-Music"
      "api-keys/data/backblaze/Chaos-Personal"
      "api-keys/data/backblaze/Chaos-Public"
      "api-keys/data/backblaze/Chaos-Media"
      "api-keys/data/backblaze/Chaos-Notes"
      "api-keys/data/backblaze/Chaos-Uploads"
      "api-keys/data/backblaze/Phoenix-Cryptidz-Storage"

      "api-keys/data/storage/webdav/Main"
      "api-keys/data/storage/webdav/Public"
      "api-keys/data/storage/webdav/Uploads"
      "api-keys/data/storage/webdav/Notes"

      "private-public-keys/data/rclone/Chaos-Media-Crypt"
      "private-public-keys/data/rclone/Chaos-Notes-Crypt"
    ];

    packages = with pkgs; [
      # for music & mail passwd files
      apacheHttpd
      # for rclone obscure config file
      rclone
    ];

    extraFunctions = ''
      replace_slash_for_sed() {
        sed "s#/#\\\/#"
      }

      simple_get_obscure() {
        rclone obscure "$(simple_get "$@")"
      }

      simple_get_replace_b2() {
        api_account=$(simple_get "$1" .keyID | replace_slash_for_sed)
        api_key=$(simple_get "$1" .applicationKey | replace_slash_for_sed)

        replace_account=''${2}_ACCOUNT
        replace_key=''${2}_KEY

        sed -i "s/$replace_account/$api_account/" "$3"
        sed -i "s/$replace_key/$api_key/" "$3"
      }

      simple_get_replace_crypt() {
        password=$(simple_get_obscure "$1" .password)
        salt=$(simple_get_obscure "$1" .salt)

        replace_password=''${2}_PASSWORD
        replace_salt=''${2}_SALT

        sed -i "s/$replace_password/$password/" "$3"
        sed -i "s/$replace_salt/$salt/" "$3"
      }

      simple_get_htpasswd() {
        if [ -f "$2" ]; then
          rm "$2"
        fi

        touch "$2"

        data=$(kv_get "$1" | base64)
        for username in $(echo "$data" | base64 -d | jq -r ".data.data | keys | .[]"); do
          password=$(echo "$data" | base64 -d | jq -r ".data.data.\"$username\"")
          htpasswd -b "$2" "$username" "$password" 2>/dev/null
        done
      }
    '';

    secrets = {
      vault_password = {
        manual = true;
      };

      webdav_main_htpasswd = {
        user = "storage";
        group = "storage";
        fetchScript = ''
          simple_get_htpasswd "/api-keys/storage/webdav/Main" "$secretFile"
        '';
      };
      webdav_uploads_htpasswd = {
        user = "storage";
        group = "storage";
        fetchScript = ''
          simple_get_htpasswd "/api-keys/storage/webdav/Uploads" "$secretFile"
        '';
      };
      webdav_notes_htpasswd = {
        user = "storage";
        group = "storage";
        fetchScript = ''
          simple_get_htpasswd "/api-keys/storage/webdav/Notes" "$secretFile"
        '';
      };

      rclone_config = {
        user = "storage";
        group = "storage";
        fetchScript = ''
          cp ${./data/rclone_config.template} "$secretFile"

          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Media" "B2_CHAOS_MEDIA" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Backups" "B2_CHAOS_BACKUPS" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Photos" "B2_CHAOS_PHOTOS" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Music" "B2_CHAOS_MUSIC" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Personal" "B2_CHAOS_PERSONAL" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Public" "B2_CHAOS_PUBLIC" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Notes" "B2_CHAOS_NOTES" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Chaos-Uploads" "B2_CHAOS_UPLOADS" "$secretFile"
          simple_get_replace_b2 "/api-keys/backblaze/Phoenix-Cryptidz-Storage" "B2_PHOENIX_CRYPTIDZ_STORAGE" "$secretFile"

          simple_get_replace_crypt "/private-public-keys/rclone/Chaos-Media-Crypt" "STORAGE_MEDIA_CRYPT" "$secretFile"
          simple_get_replace_crypt "/private-public-keys/rclone/Chaos-Notes-Crypt" "STORAGE_NOTES_CRYPT" "$secretFile"
        '';
      };
    };
  };
}
