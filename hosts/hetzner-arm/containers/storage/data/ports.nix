let
  webdav = 4200;
  http = 4300;
in {
  webdav_main = webdav + 0;
  webdav_music_ro = webdav + 1;
  webdav_uploads = webdav + 2;
  webdav_notes = webdav + 3;

  http_music = http + 0;
  http_public = http + 1;
  http_uploads_public = http + 2;
}
