{
  lib,
  config,
  ...
}: let
  inherit (lib.modules) mkIf;

  mailConfig = config.services.mailserver;
in {
  config = mkIf mailConfig.enable {
    networking.firewall = {
      allowedTCPPorts = [
        # SMTP
        25
        # Submission
        587
        # Submission w/ SSL
        465
        # IMAP
        143
        # IMAP w/ SSL
        993
        # Sieve
        4190
      ];
    };
  };
}
