{config, ...}: let
  inherit (config.services.secrets) secrets;
in {
  services.mailserver = {
    enable = true;
    fqdn = "mail.owo.monster";
    domains = ["owo.monster"];
    debugMode = true;

    sslConfig = {
      useACME = false;
      cert = "/var/lib/acme/mail.owo.monster/fullchain.pem";
      key = "/var/lib/acme/mail.owo.monster/key.pem";
    };

    spf.enable = false;

    accounts = {
      "chaos@owo.monster" = {
        passwordHashFile = "${secrets.chaos_mail_passwd.path}";
        aliases = [
          "all@owo.monster"
          "chaoticryptidz@owo.monster"
        ];
      };

      "system@owo.monster" = {
        passwordHashFile = "${secrets.system_mail_passwd.path}";
      };

      "gotosocial@owo.monster" = {
        passwordHashFile = "${secrets.gotosocial_mail_passwd.path}";
      };
    };

    extraAliasesFile = "${secrets.private_mail_aliases.path}";
  };
}
