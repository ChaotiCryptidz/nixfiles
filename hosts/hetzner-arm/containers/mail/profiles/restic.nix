{
  self,
  config,
  ...
}: let
  backupSchedules = import "${self}/data/backupSchedules.nix";
  inherit (config.services.secrets) secrets;

  mailConfig = config.services.mailserver;
in {
  services.restic.backups.mail = {
    user = "root";
    paths = [
      mailConfig.vmail.directory
      mailConfig.sieveDirectory
      mailConfig.dkim.directory
    ];

    repository = "s3:s3.eu-central-003.backblazeb2.com/Chaos-Backups/Restic/Mail";
    passwordFile = "${secrets.restic_password.path}";
    environmentFile = "${secrets.restic_env.path}";
    createWrapper = true;

    pruneOpts = ["--keep-last 60"];
    timerConfig = backupSchedules.restic.medium;
  };
}
