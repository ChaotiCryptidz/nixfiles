{pkgs, ...}: {
  services.secrets = {
    enable = true;

    vaultLogin = {
      enable = true;
      loginUsername = "hetzner-arm-container-mail";
    };

    requiredVaultPaths = [
      "api-keys/data/backblaze/Chaos-Backups"
      "api-keys/data/chaos_mail/system"
      "api-keys/data/chaos_mail/gotosocial"
      "passwords/data/mail"
      "private-public-keys/data/restic/Mail"
      "infra/data/private-mail-aliases"
    ];

    packages = with pkgs; [
      apacheHttpd
    ];

    secrets = {
      vault_password = {
        manual = true;
      };

      restic_password = {
        fetchScript = ''
          simple_get "/private-public-keys/restic/Mail" .password > "$secretFile"
        '';
      };
      restic_env = {
        fetchScript = ''
          cat << EOF > "$secretFile"
          AWS_ACCESS_KEY_ID=$(simple_get "/api-keys/backblaze/Chaos-Backups" .keyID)
          AWS_SECRET_ACCESS_KEY=$(simple_get "/api-keys/backblaze/Chaos-Backups" .applicationKey)
          EOF
        '';
      };
      private_mail_aliases = {
        fetchScript = ''
          kv_get "/infra/private-mail-aliases" | jq .data.data | jq -r 'to_entries|map("\(.key) \(.value.to)")[]' > "$secretFile"
        '';
      };
      chaos_mail_passwd = {
        user = "dovecot2";
        group = "dovecot2";
        fetchScript = ''
          password=$(simple_get "/passwords/mail" .password)
          htpasswd -nbB "" "$password" 2>/dev/null | cut -d: -f2 > "$secretFile"
        '';
      };
      system_mail_passwd = {
        user = "dovecot2";
        group = "dovecot2";
        fetchScript = ''
          password=$(simple_get "/api-keys/chaos_mail/system" .password)
          htpasswd -nbB "" "$password" 2>/dev/null | cut -d: -f2 > "$secretFile"
        '';
      };
      gotosocial_mail_passwd = {
        user = "dovecot2";
        group = "dovecot2";
        fetchScript = ''
          password=$(simple_get "/api-keys/chaos_mail/gotosocial" .password)
          htpasswd -nbB "" "$password" 2>/dev/null | cut -d: -f2 > "$secretFile"
        '';
      };
    };
  };
}
