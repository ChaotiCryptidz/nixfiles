{
  self,
  tree,
  lib,
  inputs,
  config,
  pkgs,
  ...
}: let
  inherit (lib.modules) mkMerge mkForce;
  inherit (lib.lists) flatten;

  ports = [
    # SMTP
    25
    # Submission
    587
    # Submission w/ SSL
    465
    # IMAP
    143
    # IMAP w/ SSL
    993
    # Sieve
    4190
  ];

  sharedFiles = [
    "/var/lib/acme/mail.owo.monster/fullchain.pem"
    "/var/lib/acme/mail.owo.monster/key.pem"
  ];
in {
  containers.mail = {
    autoStart = true;

    bindMounts = mkMerge (map (file: {
        "${file}" = {
          hostPath = "${file}";
        };
      })
      sharedFiles);

    specialArgs = {
      inherit inputs;
      inherit tree;
      inherit self;
    };

    config = {...}: {
      nixpkgs.pkgs = pkgs;

      imports = flatten (with tree; [
        presets.nixos.containerBase

        (with hosts.hetzner-arm.containers.mail; [
          modules.mailserver

          profiles.mailserver
          profiles.restic
        ])

        ./secrets.nix
      ]);

      systemd.tmpfiles.rules = [
        "d /var/lib/acme - root root"
        "d /var/lib/acme/mail.owo.monster - root root"
      ];

      networking.firewall = {
        enable = mkForce false;
      };

      home-manager.users.root.home.stateVersion = "24.05";
      system.stateVersion = "24.05";
    };
  };

  # ssl for mail
  services.nginx = {
    enable = true;
    virtualHosts."mail.owo.monster" = {
      serverName = "mail.owo.monster";
      serverAliases = ["owo.monster"];
      forceSSL = true;
      enableACME = true;
      acmeRoot = "/var/lib/acme/acme-challenge";
    };
  };

  networking.firewall = {
    allowedTCPPorts = ports;
    allowedUDPPorts = ports;
  };
}
