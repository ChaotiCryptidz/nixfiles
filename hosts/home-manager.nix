{self, ...} @ inputs: let
  mkTree = inputs.tree-input.tree;
  metaTree = mkTree ((import ../treeConfig.nix {}) // {inherit inputs;});
  tree = metaTree.impure;

  nixpkgs = inputs.nixpkgs-unstable;
  home-manager = inputs.home-manager-unstable;

  patchedInputs =
    inputs
    // {
      # set these to the correct versions from inputs
      inherit nixpkgs home-manager;
    };

  specialArgs = {
    inherit self;
    tree = metaTree.impure;
    pureTree = metaTree.pure;
    inputs = patchedInputs;
  };

  defaultModules = [
    tree.profiles.home-manager.base
  ];

  pkgsFor = system:
    import nixpkgs {
      inherit system;
      config.allowUnfree = true;
      overlays = [
        (import ../overlay)
      ];
    };
in rec {
  "blahaj-chaos" = home-manager.lib.homeManagerConfiguration {
    pkgs = pkgsFor "x86_64-linux";

    extraSpecialArgs = specialArgs;
    modules = defaultModules ++ [./blahaj/chaos.nix];
  };
}
