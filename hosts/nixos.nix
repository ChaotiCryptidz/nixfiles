{
  self,
  nixpkgs-unstable,
  ...
} @ inputs: let
  mkTree = inputs.tree-input.tree;
  metaTree = mkTree ((import ../treeConfig.nix {}) // {inherit inputs;});
  tree = metaTree.impure;

  patchedInputs =
    inputs
    // {
      # set these to the correct versions from inputs
      nixpkgs = inputs.nixpkgs-unstable;
      home-manager = inputs.home-manager-unstable;
    };

  defaultSpecialArgs = {
    inherit self;
    tree = metaTree.impure;
    pureTree = metaTree.pure;
    inputs = patchedInputs;
  };

  defaultModules = [
    tree.profiles.nixos.base

    inputs.home-manager-unstable.nixosModules.home-manager

    inputs.vaultui.nixosModules.default

    tree.modules.nixos.rcloneServe
    tree.modules.nixos.rcloneSync
    tree.modules.nixos.secrets
    tree.modules.nixos.encryptedDrive
  ];

  nixosUnstableSystem = nixpkgs-unstable.lib.nixosSystem;
in rec {
  lappy-t495 = nixosUnstableSystem {
    specialArgs =
      defaultSpecialArgs;
    system = "x86_64-linux";
    modules = defaultModules ++ [./lappy-t495/lappy-t495.nix];
  };

  hetzner-arm = nixosUnstableSystem {
    specialArgs =
      defaultSpecialArgs;
    system = "aarch64-linux";
    modules = defaultModules ++ [./hetzner-arm/hetzner-arm.nix];
  };

  raspberry-pi5 = nixosUnstableSystem {
    specialArgs =
      defaultSpecialArgs;
    system = "aarch64-linux";
    modules = defaultModules ++ [./raspberry-pi5/raspberry-pi5.nix];
  };
}
