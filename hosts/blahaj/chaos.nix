{tree, ...}: {
  imports = with tree.presets.home-manager; [
    devBase
  ];

  home.username = "chaos";
  home.homeDirectory = "/home/chaos";
  home.stateVersion = "24.05";
}
