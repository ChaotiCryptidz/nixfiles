{...}: {
  services.secrets = {
    enable = true;
    secrets = {
      usb_encryption_passphrase = {
        manual = true;
      };

      music_stream_password = {
        user = "chaos";
        group = "users";
        fetchScript = ''
          simple_get "/api-keys/music-stream" .password > "$secretFile"
        '';
      };
    };
  };
}
