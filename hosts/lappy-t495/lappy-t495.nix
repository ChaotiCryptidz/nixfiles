{
  lib,
  tree,
  ...
}: let
  inherit (lib.lists) flatten;
in {
  imports = flatten (with tree; [
    users.root
    users.chaos
    users.guest

    (with tree.presets.nixos; [
      desktopGui
      laptop
      gaming
      encryptedUSB
      kernelLatest
    ])

    (with tree.profiles.nixos; [
      cross.arm64
      remoteBuilders
    ])

    (with presets.home-manager.by-user; [
      root.base
      chaos.base
      chaos.guiDevDesktop
      chaos.gaming
    ])

    ./secrets.nix
    ./hardware.nix
  ]);

  home-manager.users.guest = {
    imports = flatten (with tree; [
      (with profiles.home-manager; [
        base
      ])

      (with presets.home-manager; [
        gaming
        guiDesktop
      ])
    ]);
    home.stateVersion = "24.05";
  };

  networking.firewall = {
    enable = true;
    allowPing = true;
  };

  networking.hostName = "lappy-t495";
  time.timeZone = "Europe/London";

  system.stateVersion = "24.05";
  home-manager.users.root.home.stateVersion = "24.05";
  home-manager.users.chaos.home.stateVersion = "24.05";
}
