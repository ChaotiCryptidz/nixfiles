{tree, ...}: {
  imports = with tree; [
    presets.nixos.encryptedDrive
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    initrd.availableKernelModules = [
      # defaults from nixos-generate-config
      "nvme"
      "ehci_pci"
      "xhci_pci"
      "usb_storage"
      "sd_mod"
      "sdhci_pci"
    ];
    kernelModules = ["kvm-amd"];
  };

  hardware.cpu.amd.updateMicrocode = true;

  services.tlp.settings = {
    RUNTIME_PM_BLACKLIST = "05:00.3 05:00.4";
  };
}
