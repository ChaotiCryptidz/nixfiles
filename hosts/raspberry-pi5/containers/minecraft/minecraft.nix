{
  self,
  tree,
  lib,
  inputs,
  config,
  pkgs,
  ...
}: let
  inherit (lib.modules) mkForce;
in {
  containers.minecraft = {
    autoStart = true;

    specialArgs = {
      inherit inputs;
      inherit tree;
      inherit self;
    };

    config = {...}: {
      nixpkgs.pkgs = pkgs;

      imports = with tree; [
        presets.nixos.containerBase
      ];

      networking.firewall = {
        enable = mkForce false;
      };

      users.users.minecraft = {
        description = "Minecraft server service user";
        home = "/var/lib/minecraft";
        createHome = true;
        isSystemUser = true;
        group = "minecraft";
      };
      users.groups.minecraft = {};

      systemd.sockets.minecraft-server = {
        bindsTo = ["minecraft-server.service"];
        socketConfig = {
          ListenFIFO = "/run/minecraft-server.stdin";
          SocketMode = "0660";
          SocketUser = "minecraft";
          SocketGroup = "minecraft";
          RemoveOnStop = true;
          FlushPending = true;
        };
      };

      systemd.services.minecraft-server = let
        stopScript = pkgs.writeShellScript "minecraft-server-stop" ''
          echo stop > /run/minecraft-server.stdin

          while kill -0 "$1" 2> /dev/null; do
            sleep 1s
          done
        '';
      in {
        description = "Minecraft Server Service";
        wantedBy = ["multi-user.target"];
        requires = ["minecraft-server.socket"];
        after = ["network.target" "minecraft-server.socket"];

        serviceConfig = {
          ExecStart = "${pkgs.jdk8.jre}/bin/java -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -Xmx4096M -Xms2048M -Djava.security.properties=java.security -jar forge-1.7.10-10.13.4.1558-1.7.10-universal.jar nogui";
          ExecStop = "${stopScript} $MAINPID";
          Restart = "always";
          User = "minecraft";
          WorkingDirectory = "/var/lib/minecraft";

          StandardInput = "socket";
          StandardOutput = "journal";
          StandardError = "journal";

          # Hardening
          CapabilityBoundingSet = [""];
          DeviceAllow = [""];
          LockPersonality = true;
          PrivateDevices = true;
          PrivateTmp = true;
          PrivateUsers = true;
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHome = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          ProtectKernelTunables = true;
          ProtectProc = "invisible";
          RestrictAddressFamilies = ["AF_INET" "AF_INET6"];
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          SystemCallArchitectures = "native";
          UMask = "0077";
        };
      };

      home-manager.users.root.home.stateVersion = "24.05";
      system.stateVersion = "24.05";
    };
  };

  networking.firewall = {
    allowedUDPPorts = [25565];
    allowedTCPPorts = [25565];
  };
}
