{
  pkgs,
  inputs,
  ...
}: let
  kodiForkPkg = inputs.nixpkgs-kodi-inputstream-adaptive-fork.legacyPackages.${pkgs.system}.kodi;

  kodiForkPkgPatched =
    (kodiForkPkg.override {
      x11Support = true;
      waylandSupport = true;
      gbmSupport = true;
    })
    .overrideAttrs (oldAttrs: {
      patches =
        oldAttrs.patches
        ++ [
          #./rbp-000-add-pi-support.patch
        ];
    });

  kodiPackage = kodiForkPkgPatched.withPackages (kodiPkgs:
    with kodiPkgs; [
      pvr-iptvsimple
      inputstreamhelper
      inputstream-adaptive
      inputstream-rtmp
      visualization-matrix
      visualization-spectrum
    ]);
in {
  nixpkgs.overlays = [
    (_final: _prev: {
      #ffmpeg = final.ffmpeg-pi;
    })
  ];

  hardware.graphics.package = pkgs.mesa.overrideAttrs (oA: {
    inherit (oA) patches;
  });

  environment.systemPackages = [
    kodiPackage
  ];

  services.xserver = {
    enable = true;
    desktopManager.kodi = {
      enable = true;
      package = kodiPackage;
    };
    extraConfig = ''
      Section "OutputClass"
        Identifier "vc4"
        MatchDriver "vc4"
        Driver "modesetting"
        Option "PrimaryGPU" "true"
      EndSection
    '';
  };

  networking.firewall = {
    allowedTCPPorts = [8080];
    allowedUDPPorts = [8080];
  };

  home-manager.users.chaos = let
    widevine = pkgs.widevine-aarch64-4k;
  in {
    home.file.widevine-lib = {
      source = "${widevine}/share/google/chrome/WidevineCdm/_platform_specific/linux_arm64/libwidevinecdm.so";
      target = ".kodi/cdm/libwidevinecdm.so";
    };
    home.file.widevine-manifest = {
      source = "${widevine}/share/google/chrome/WidevineCdm/manifest.json";
      target = ".kodi/cdm/manifest.json";
    };
  };
}
