{
  lib,
  tree,
  ...
}: let
  inherit (lib.lists) flatten forEach;
  inherit (lib.modules) mkForce;
in {
  imports = flatten (with tree; [
    users.root
    users.chaos

    (with tree.profiles.nixos; [
      sshd

      tor
      wifiHardware

      nixGC
      serverExtras
    ])

    (with tree.presets.nixos; [
      serverBase

      desktopGui
    ])

    (with presets.home-manager.by-user; [
      root.base

      chaos.base
      chaos.guiDesktop
    ])

    (with tree.hosts.raspberry-pi5.profiles; [
      #kodi
      docker
    ])

    (forEach [
      "minecraft"
    ] (name: ./containers + "/${name}/${name}.nix"))

    ./hardware.nix
  ]);

  services.displayManager = {
    autoLogin = {
      enable = true;
      user = "chaos";
    };
  };

  home-manager.users.root.imports = with tree.profiles.home-manager; [dev.all];
  home-manager.users.chaos.imports = with tree.profiles.home-manager; [dev.all];

  services.xserver.displayManager.gdm.autoLogin.delay = 5;

  networking.firewall = {
    enable = true;

    allowPing = true;
    checkReversePath = "loose";

    allowedTCPPorts = [22 9981 9982];
  };

  networking.nat = {
    enable = true;
    internalInterfaces = ["ve-+"];
    externalInterface = "end0";
  };

  networking.networkmanager.enable = mkForce false;
  networking.useDHCP = mkForce false;

  networking.hostName = "raspberry-pi5";
  time.timeZone = "Europe/London";

  system.stateVersion = "24.05";
  home-manager.users.root.home.stateVersion = "24.05";
  home-manager.users.chaos.home.stateVersion = "24.05";
}
