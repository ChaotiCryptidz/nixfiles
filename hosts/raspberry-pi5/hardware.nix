{
  tree,
  pkgs,
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkForce;
  inherit (lib.lists) forEach foldl';
  inherit (lib.attrsets) recursiveUpdate mapAttrsToList;
  inherit (builtins) toFile concatStringsSep;

  edk2 = pkgs.fetchzip {
    url = "https://github.com/worproject/rpi5-uefi/releases/download/v0.3/RPi5_UEFI_Release_v0.3.zip";
    hash = "sha256-bjEvq7KlEFANnFVL0LyexXEeoXj7rHGnwQpq09PhIb0=";
    stripRoot = false;
  };

  fwFiles = let
    piBootFw = "${pkgs.raspberrypifw}/share/raspberrypi/boot";

    piBootFwFiles = foldl' recursiveUpdate {} (forEach [
        "bootcode.bin"
        "start.elf"

        "fixup_cd.dat"
        "fixup.dat"
        "fixup_db.dat"
        "fixup_x.dat"
        "fixup4cd.dat"
        "fixup4.dat"
        "fixup4db.dat"
        "fixup4x.dat"

        "start_cd.elf"
        "start_db.elf"
        "start.elf"
        "start_x.elf"
        "start4cd.elf"
        "start4db.elf"
        "start4.elf"
        "start4x.elf"

        "bcm2712-rpi-5-b.dtb"
        "overlays/vc4-kms-v3d-pi5.dtbo"
      ] (file: {
        "${file}" = "${piBootFw}/${file}";
      }));
  in
    {
      "config.txt" = toFile "config.txt" ''
        armstub=RPI_EFI.fd
        device_tree_address=0x1f0000
        device_tree_end=0x210000

        # Makes GPU work?
        dtoverlay=vc4-kms-v3d-pi5
        cma=256M@256M

        # Force 32 bpp framebuffer allocation.
        framebuffer_depth=32

        # Enable 4k60
        hdmi_enable_4kp60=1

        # Disable compensation for displays with overscan.
        disable_overscan=1

        # Force maximum USB power regardless of the power supply.
        usb_max_current_enable=1

        # Force maximum CPU speed.
        force_turbo=1
        arm_boost=1
      '';
      # UEFI Files
      "RPI_EFI.fd" = "${edk2}/RPI_EFI.fd";
      # Using a more updated dtb
      # "bcm2712-rpi-5-b.dtb" = "${edk2}/bcm2712-rpi-5-b.dtb";
    }
    // piBootFwFiles;
in {
  imports = with tree; [
    presets.nixos.serverEncryptedDrive
  ];

  nixpkgs.overlays = [
    (_final: prev: {
      makeModulesClosure = x:
        prev.makeModulesClosure (x // {allowMissing = true;});
    })
  ];

  services.xserver.videoDrivers = [
    "modesetting"
    "fbdev"
  ];

  services.kmscon = {
    enable = true;
    fonts = [
      {
        name = "Comic Code";
        package = pkgs.comic-code;
      }
    ];
  };

  environment.systemPackages = with pkgs; [
    config.boot.kernelPackages.cpupower
    raspberrypi-utils
    raspberrypi-eeprom
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_rpi4;
    supportedFilesystems = mkForce ["vfat"];
    kernelParams = [
      "kunit.enable=0"

      "console=ttyS0,115200n8"
      "console=tty0"

      "ip=192.168.178.26::192.168.178.1:255.255.255.0:raspberry:end0:any"
      "boot.shell_on_fail"
      "nohibernate"
      "loglevel=4"
    ];
    loader = {
      systemd-boot = {
        enable = true;
        extraFiles = fwFiles;
      };
      efi.canTouchEfiVariables = false;

      grub.enable = false;
      generic-extlinux-compatible.enable = false;
    };
    initrd.availableKernelModules = [
      "nvme"
      "ehci_pci"
      "xhci_pci"
      "usb_storage"
      "sd_mod"
      "sdhci_pci"
      "usbhid"
      "vc4"
      "pcie_brcmstb"
      "reset-raspberrypi"
    ];
  };

  system.build.firmware = pkgs.runCommand "firmware" {} ''
    mkdir $out

    ${concatStringsSep "\n" (
      mapAttrsToList (filename: path: ''
        mkdir -p $(dirname $out/${filename})
        cp ${path} $out/${filename}
      '')
      fwFiles
    )}
  '';

  systemd.network = {
    enable = true;
    networks."end0" = {
      name = "end0";
      networkConfig.DHCP = "no";
      address = [
        # v4
        "192.168.178.26/24"

        # v6
        "fd00::2ecf:67ff:fe74:940d/64"
        "2a02:8012:7883:0:2ecf:67ff:fe74:940d/64"
      ];

      routes = [
        # v4
        {
          Destination = "192.168.178.1";
        }
        {
          Gateway = "192.168.178.1";
          GatewayOnLink = true;
        }
        # v6
        {
          Gateway = "fe80::1";
        }
      ];
    };
  };
}
