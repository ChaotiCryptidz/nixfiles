inputs: {
  nixosConfigurations = import ./nixos.nix inputs;
  homeConfigurations = import ./home-manager.nix inputs;
}
