#!/usr/bin/env nix-shell
#!nix-shell -i bash -p nixFlakes

nix --experimental-features "nix-command flakes" "$@"