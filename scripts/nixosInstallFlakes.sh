#!/usr/bin/env nix-shell
#!nix-shell -i bash -p nixFlakes

nixos-install --experimental-features "nix-command flakes" "$@"