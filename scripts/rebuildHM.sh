#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPO_ROOT="${SCRIPT_DIR}/.."
cd $REPO_ROOT

MODE=""
if [ -z "${1-}" ]; then
  MODE=switch
fi

if command -v nom &> /dev/null && [ "$MODE" == "switch" ]; then
  nix build "${REPO_ROOT}#homeConfigurations.$(hostname)-$(whoami).activationPackage" "$@" --keep-failed --log-format internal-json -v |& nom --json
  ./result/activate
else 
  echo "Install nix-output-monitor for better output"
  nix run .#home-manager -- --flake "${REPO_ROOT}#$(hostname)-$(whoami)" "$MODE" "$@"
fi