#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd $(git rev-parse --show-toplevel)

DEFAULT_HOST="root@192.168.178.26"
TARGET_HOST=${HOST:-${DEFAULT_HOST}}
nixos-rebuild switch --flake .#raspberry-pi5 --target-host "$TARGET_HOST" --impure --no-build-nix --fast --use-substitutes -s "$@" 