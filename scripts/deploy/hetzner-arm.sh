#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd $(git rev-parse --show-toplevel)

DEFAULT_HOST="root@hetzner-arm.servers.genderfucked.monster"
TARGET_HOST=${HOST:-${DEFAULT_HOST}}
nixos-rebuild switch --flake .#hetzner-arm --target-host "$TARGET_HOST" --no-build-nix --fast --use-substitutes -s "$@" 