{tree, ...}: {
  imports = with tree.profiles.nixos; [
    gaming.steam
    logitech
  ];
}
