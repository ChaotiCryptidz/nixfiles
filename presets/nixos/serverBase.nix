{tree, ...}: {
  imports = with tree; [
    users.root
    presets.home-manager.by-user.root.minimalServer

    profiles.nixos.sshd

    profiles.nixos.nixGC
    profiles.nixos.serverExtras
  ];

  networking.firewall = {
    enable = true;
    allowPing = true;
    checkReversePath = "loose";
    allowedTCPPorts = [22];
  };

  # TODO: Better DNS setup
  services.resolved.enable = false;
  environment.etc."resolv.conf".text = ''
    nameserver 8.8.8.8
    nameserver 8.8.4.4
  '';

  boot.kernel.sysctl = {
    "net.core.default_qdisc" = "fq";
    "net.ipv4.tcp_congestion_control" = "bbr";
    "fs.inotify.max_user_watches" = 1024 * 64 * 16;
  };

  time.timeZone = "Europe/London";
}
