{
  lib,
  tree,
  ...
}: let
  inherit (lib.lists) flatten;
in {
  imports = flatten (with tree; [
    (with profiles.nixos; [
      gui.base
      gui.environments.gnome

      sound.base
      sound.pipewire

      wifiHardware

      mullvad
    ])

    (with presets.home-manager; [
      by-user.chaos.guiDesktop
    ])
  ]);
}
