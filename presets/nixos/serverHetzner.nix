{
  self,
  config,
  modulesPath,
  pkgs,
  lib,
  ...
}: let
  inherit (lib.lists) optionals;
  inherit (lib.modules) mkForce;

  inherit (pkgs) system;

  serverIPs = import "${self}/data/serverIPs.nix";

  inherit (config.networking) hostName;
  hostServerIPs = serverIPs.${hostName};

  gateway = "172.31.1.1";
  netmask = "255.255.255.255";
in {
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  systemd.services = {
    systemd-networkd-wait-online.enable = mkForce false;
  };

  networking = {
    usePredictableInterfaceNames = true;
    useDHCP = false;
    dhcpcd.enable = false;
    useNetworkd = true;
  };

  boot.initrd.kernelModules = [
    "virtio_gpu"
    "virtio_pci"
    "virtio_net"
    "virtio_scsi"
  ];

  boot.kernelParams =
    [
      "ip=${hostServerIPs.ipv4}::${gateway}:${netmask}:${hostName}:enp1s0:any"
      "boot.shell_on_fail"
      "nohibernate"
      "loglevel=4"
    ]
    ++ (lib.optionals (system == "aarch64-linux") ["console=tty"]);

  systemd.network = {
    enable = true;
    networks."enp1s0" = {
      name = "enp1s0";
      networkConfig.DHCP = "no";
      address = [
        # v4
        "${hostServerIPs.ipv4}/32"

        # v6
        "${hostServerIPs.ipv6}/64"
      ];

      routes = [
        # v4
        {
          Destination = gateway;
        }
        {
          Gateway = gateway;
          GatewayOnLink = true;
        }
        # v6
        {
          Gateway = "fe80::1";
        }
      ];
    };
  };
}
