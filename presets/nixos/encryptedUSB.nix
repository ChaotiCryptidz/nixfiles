{tree, ...}: {
  imports = with tree.profiles.nixos; [
    usbAutoMount
  ];

  home-manager.users.chaos = {
    imports = with tree.profiles.home-manager; [
      sshUSB
    ];
  };
}
