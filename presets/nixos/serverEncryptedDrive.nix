{tree, ...}: {
  imports = with tree; [
    profiles.nixos.sshd
  ];

  boot.encryptedDrive = {
    enable = true;
    mode = "ssh";
    allowPasswordDecrypt = true;
  };
}
