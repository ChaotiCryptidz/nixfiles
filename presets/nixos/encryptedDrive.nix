{...}: {
  boot.encryptedDrive = {
    enable = true;
    mode = "encrypted-usb";
    allowPasswordDecrypt = true;
  };
}
