{
  tree,
  inputs,
  ...
}: {
  imports =
    (with tree; [
      profiles.nixos.base

      users.root
      presets.home-manager.by-user.root.minimalServer

      modules.nixos.rcloneServe
      modules.nixos.rcloneSync
      modules.nixos.secrets
    ])
    ++ [
      # Default modules which are usually included in nixos.nix
      inputs.home-manager-unstable.nixosModules.home-manager
      inputs.vaultui.nixosModules.default
    ];

  networking.firewall = {
    enable = true;
    allowPing = true;
    checkReversePath = "loose";
  };

  # TODO: Better DNS setup
  services.resolved.enable = false;
  environment.etc."resolv.conf".text = ''
    nameserver 8.8.8.8
    nameserver 8.8.4.4
  '';

  time.timeZone = "Europe/London";
}
