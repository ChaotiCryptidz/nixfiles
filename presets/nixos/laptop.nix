{tree, ...}: {
  imports = with tree.profiles.nixos; [
    laptop

    connectivity.networkManager
    connectivity.iOS

    wifiHardware

    tor
  ];

  boot.loader.systemd-boot = {
    editor = true;
    memtest86.enable = true;
    netbootxyz.enable = true;
  };

  services.fwupd.enable = true;

  services.fstrim.enable = true;
  systemd.services.NetworkManager-wait-online.enable = false;
}
