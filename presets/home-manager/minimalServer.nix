{tree, ...}: {
  imports = with tree.profiles.home-manager; [
    base.zsh
    base.age-encryption
    dev.small
  ];
}
