{tree, ...}: {
  imports = with tree.profiles.home-manager; [
    base
    dev.small
  ];
}
