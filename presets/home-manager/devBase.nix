{tree, ...}: {
  imports = with tree.profiles.home-manager; [
    dev.all

    programming.editors.nano
    programming.languages.rust
    programming.languages.nix
  ];
}
