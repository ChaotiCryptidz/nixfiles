{
  tree,
  lib,
  ...
}: let
  inherit (lib.lists) flatten;
in {
  imports = flatten (with tree.profiles.home-manager; [
    apps.obs
    (with gaming; [
      steam
      lutris
      arrpc
      minecraft
    ])
  ]);
}
