{tree, ...}: {
  home-manager.users.root = {
    imports = with tree.presets.home-manager; [
      base
    ];
  };
}
