{tree, ...}: {
  home-manager.users.chaos = {
    imports = with tree.presets.home-manager; [
      gaming
    ];
  };
}
