{
  tree,
  lib,
  ...
}: let
  inherit (lib.lists) flatten;
in {
  home-manager.users.chaos = {
    imports = flatten (with tree; [
      (with presets.home-manager; [
        guiDesktop
        devGui
      ])

      (with profiles.home-manager; [
        reversing
      ])
    ]);
  };
}
