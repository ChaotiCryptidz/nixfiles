{tree, ...}: {
  imports = with tree.profiles.home-manager; [
    gui.base
    gui.environments.gnome

    homeFolders
    musicLibrary

    apps.fileRoller
    apps.nautilus
    apps.pavucontrol
    apps.mpv

    apps.firefox
    apps.telegram
    apps.thunderbird
    apps.toot-cli

    apps.obsidian
    apps.libreoffice

    apps.nicotine-plus
    apps.musicutil

    apps.mullvad
    apps.aria2
    apps.rclone
    apps.restic
  ];
}
