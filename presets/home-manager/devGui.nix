{
  lib,
  tree,
  ...
}: let
  inherit (lib.lists) flatten;
in {
  imports = flatten (with tree; [
    (with presets.home-manager; [
      devBase
    ])

    (with profiles.home-manager; [
      programming.editors.vscode
    ])
  ]);
}
